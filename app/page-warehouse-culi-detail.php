
<!DOCTYPE html>
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
    <head>
        <meta charset="utf-8">

        <title>Trane Inventory Management System</title>

        <meta name="description" content="uAdmin is a Professional, Responsive and Flat Admin Template created by pixelcave and published on Themeforest">
        <meta name="author" content="pixelcave">
        <meta name="robots" content="noindex, nofollow">

        <meta name="viewport" content="width=device-width,initial-scale=1">

        <!-- Icons -->
        <!-- The following icons can be replaced with your own, they are used by desktop and mobile browsers -->
        <link rel="shortcut icon" href="img/favicon.ico">
        <link rel="apple-touch-icon" href="img/icon57.png" sizes="57x57">
        <link rel="apple-touch-icon" href="img/icon72.png" sizes="72x72">
        <link rel="apple-touch-icon" href="img/icon76.png" sizes="76x76">
        <link rel="apple-touch-icon" href="img/icon114.png" sizes="114x114">
        <link rel="apple-touch-icon" href="img/icon120.png" sizes="120x120">
        <link rel="apple-touch-icon" href="img/icon144.png" sizes="144x144">
        <link rel="apple-touch-icon" href="img/icon152.png" sizes="152x152">
        <!-- END Icons -->

        <!-- Stylesheets -->
        <!-- Bootstrap is included in its original form, unaltered -->
        <link rel="stylesheet" href="css/bootstrap.css">

        <!-- Related styles of various javascript plugins -->
        <link rel="stylesheet" href="css/plugins.css">

        <!-- The main stylesheet of this template. All Bootstrap overwrites are defined in here -->
        <link rel="stylesheet" href="css/main.css">

        <!-- Load a specific file here from css/themes/ folder to alter the default theme of the template -->

        <!-- The themes stylesheet of this template (for using specific theme color in individual elements - must included last) -->
        <link rel="stylesheet" href="css/themes.css">
        <!-- END Stylesheets -->

        <!-- Modernizr (browser feature detection library) & Respond.js (Enable responsive CSS code on browsers that don't support it, eg IE8) -->
        <script src="js/vendor/modernizr-respond.min.js"></script>
    </head>

    <!-- Add the class .fixed to <body> for a fixed layout on large resolutions (min: 1200px) -->
    <!-- <body class="fixed"> -->
    <body>
        <!-- Page Container -->
        <div id="page-container">
            <!-- Header -->
            <!-- Add the class .navbar-fixed-top or .navbar-fixed-bottom for a fixed header on top or bottom respectively -->
            <!-- <header class="navbar navbar-inverse navbar-fixed-top"> -->
            <!-- <header class="navbar navbar-inverse navbar-fixed-bottom"> -->
            <?php include 'header.php'; ?>
            <?php include 'user/warehouse/container1-detail.php'; ?>
            <!-- Inner Container -->
        </div>
        <!-- END Page Container -->

        <!-- Scroll to top link, check main.js - scrollToTop() -->
        <a href="javascript:void(0)" id="to-top"><i class="fa fa-chevron-up"></i></a>

        

        <!-- Excanvas for canvas support on IE8 -->
        <!--[if lte IE 8]><script src="js/helpers/excanvas.min.js"></script><![endif]-->

        <!-- Include Jquery library from Google's CDN but if something goes wrong get Jquery from local file (Remove 'http:' if you have SSL) -->
        <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
        <script>!window.jQuery && document.write(decodeURI('%3Cscript src="js/vendor/jquery-1.11.1.min.js"%3E%3C/script%3E'));</script>

        <!-- Bootstrap.js -->
        <script src="js/vendor/bootstrap.min.js"></script>

        <!-- Jquery plugins and custom javascript code -->
        <script src="js/plugins.js"></script>
        <script src="js/main.js"></script>

        <!-- Javascript code only for this page -->
        <script>
            $(function () {

                // Hold our table to a variable
                var exampleDatatable = $('#example-editable-datatables');

                /*
                 * Function for handing the data after a cell has been edited
                 *
                 * From here you can send the data with ajax (for example) to handle in your backend
                 *
                 */
                var reqHandle = function (value, settings) {

                    // this, the edited td element
                    console.log(this);

                    // $(this).attr('id'), get the id of the edited td
                    console.log($(this).attr('id'));

                    // $(this).parent('tr').attr('id'), get the id of the row
                    console.log($(this).parent('tr').attr('id'));

                    // value, the new value the user submitted
                    console.log(value);

                    // settings, the settings of jEditable
                    console.log(settings);

                    // Here you can send and handle the data in your backend
                    // ...

                    // For this example, just return the data the user submitted
                    return(value);
                };

                /*
                 * Function for initializing jEditable handlers to the table
                 *
                 * For advance usage you can check http://www.appelsiini.net/projects/jeditable
                 *
                 */
                var initEditable = function (rowID) {

                    // Hold the elements that the jEditable will be initialized
                    var elements;

                    // If we don't have a rowID apply to all td elements with .editable-td class
                    if (!rowID)
                        elements = $('td.editable-td', editableTable.fnGetNodes());
                    else
                        elements = $('td.editable-td', editableTable.fnGetNodes(rowID));

                    elements.editable(reqHandle, {
                        "callback": function (sValue, y) {
                            // Little fix for responsive table after edit
                            exampleDatatable.css('width', '100%');
                        },
                        "submitdata": function (value, settings) {
                            // Sent some extra data
                            return {
                                "row_id": this.parentNode.getAttribute('id'),
                                "column": editableTable.fnGetPosition(this)[2]
                            };
                        },
                        indicator: '<i class="fa fa-spinner fa-spin"></i>',
                        cssclass: 'remove-margin',
                        submit: 'Ok',
                        cancel: 'Cancel'
                    });
                };

                // Initialize Datatables
                var editableTable = exampleDatatable.dataTable({
                    order: [[1, 'desc']],
                    columnDefs: [{orderable: false, targets: [0]}]
                });
                $('.dataTables_filter input').attr('placeholder', 'Search');

                // Initialize jEditable
                initEditable();

                // Handle rows deletion
                delHandle();

                // Handle new rows
                addHandle();
            });
        </script>
    </body>
</html>
