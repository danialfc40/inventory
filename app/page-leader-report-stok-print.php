<?php
    require('pdf/fpdf.php');
    class PDF extends FPDF {
        function Header() {
            $this->Image('pdf/logo.png',10,8,33);
        }
        function Footer() {
            $this->SetY(-15);
            $this->SetFont('Arial','I',8);
            $this->Cell(0,10,''.$this->PageNo().'',0,0,'C');
        }
    }
    require_once('config/config.php');
    $sql    = "SELECT * FROM barang";  
    $query  = mysqli_query($link, $sql);
    $pdf    = new PDF('P','mm','A4');
    $pdf->AddPage();
    $pdf->AliasNbPages();
    $pdf->SetAutoPageBreak(false);
    $pdf->SetAuthor('INSERT AUTHOR');
    $pdf->SetTitle('INSERT DOC TITLE');
    $pdf->SetFont('Arial','B',14);
    $pdf->Cell(-10,7,'                                                  LAPORAN STOK BARANG');
    $pdf->SetFont('Arial','B',10);
    $date = date("F j, Y");
    $pdf->Cell(10,35,'          REPORT DATE : '.$date);
    $pdf->SetDrawColor(0, 0, 0);
    $pdf->SetFillColor(170, 170, 170);
    $pdf->setFont("Arial","B","9");
    $pdf->setXY(10, 40); 
    $pdf->Cell(8, 10, "NO.", 1, 0, "L", 1); 
    $pdf->Cell(40, 10, "PART NO.", 1, 0, "L", 1);
    $pdf->Cell(60, 10, "DESCRIPTION", 1, 0, "L", 1);
    $pdf->Cell(10, 10, "QTY", 1, 0, "L", 1);  
    $y = 50;
    $x = 10;  
    $pdf->setXY($x, $y);
    $pdf->setFont("Arial","","9");
    $no=1; 
    while($row = mysqli_fetch_array($query)) {
            $id = $row['id_barang'];
            $sql2 = "SELECT * FROM detail_barang WHERE id_barang = '$id'";
            $query2 = mysqli_query($link, $sql2);
            $row2 = mysqli_fetch_array($query2);
            $pdf->Cell(8, 8, $no++, 1);
            $pdf->Cell(40, 8, $id, 1);
            $pdf->Cell(60, 8, $row['nama_barang'], 1);
            $pdf->Cell(10, 8, $row2['qty'], 1);
            $y += 8;
            if ($y > 260) {
                $pdf->AddPage();
                $y = 40; 
            }
            $pdf->setXY($x, $y);
    }
    $pdf->Output();
?>