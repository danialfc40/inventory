
<!DOCTYPE html>
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
    <head>
        <meta charset="utf-8">

        <title>Trane Inventory Management System</title>

        <meta name="description" content="uAdmin is a Professional, Responsive and Flat Admin Template created by pixelcave and published on Themeforest">
        <meta name="author" content="pixelcave">
        <meta name="robots" content="noindex, nofollow">

        <meta name="viewport" content="width=device-width,initial-scale=1">

        <!-- Icons -->
        <!-- The following icons can be replaced with your own, they are used by desktop and mobile browsers -->
        <link rel="shortcut icon" href="img/favicon.ico">
        <link rel="apple-touch-icon" href="img/icon57.png" sizes="57x57">
        <link rel="apple-touch-icon" href="img/icon72.png" sizes="72x72">
        <link rel="apple-touch-icon" href="img/icon76.png" sizes="76x76">
        <link rel="apple-touch-icon" href="img/icon114.png" sizes="114x114">
        <link rel="apple-touch-icon" href="img/icon120.png" sizes="120x120">
        <link rel="apple-touch-icon" href="img/icon144.png" sizes="144x144">
        <link rel="apple-touch-icon" href="img/icon152.png" sizes="152x152">
        <!-- END Icons -->

        <!-- Stylesheets -->
        <!-- Bootstrap is included in its original form, unaltered -->
        <link rel="stylesheet" href="css/bootstrap.css">

        <!-- Related styles of various javascript plugins -->
        <link rel="stylesheet" href="css/plugins.css">

        <!-- The main stylesheet of this template. All Bootstrap overwrites are defined in here -->
        <link rel="stylesheet" href="css/main.css">

        <!-- Load a specific file here from css/themes/ folder to alter the default theme of the template -->

        <!-- The themes stylesheet of this template (for using specific theme color in individual elements - must included last) -->
        <link rel="stylesheet" href="css/themes.css">
        <!-- END Stylesheets -->

        <!-- Modernizr (browser feature detection library) & Respond.js (Enable responsive CSS code on browsers that don't support it, eg IE8) -->
        <script src="js/vendor/modernizr-respond.min.js"></script>
    </head>

    <!-- Add the class .fixed to <body> for a fixed layout on large resolutions (min: 1200px) -->
    <!-- <body class="fixed"> -->
    <body>
        <!-- Page Container -->
        <div id="page-container">
            <!-- Header -->
            <!-- Add the class .navbar-fixed-top or .navbar-fixed-bottom for a fixed header on top or bottom respectively -->
            <!-- <header class="navbar navbar-inverse navbar-fixed-top"> -->
            <!-- <header class="navbar navbar-inverse navbar-fixed-bottom"> -->
            <?php include 'header.php'; ?>
            <?php include 'user/part/container9-detail.php'; ?>
            <!-- Inner Container -->
        </div>
        <!-- END Page Container -->

        <!-- Scroll to top link, check main.js - scrollToTop() -->
        <a href="javascript:void(0)" id="to-top"><i class="fa fa-chevron-up"></i></a>

        

        <!-- Excanvas for canvas support on IE8 -->
        <!--[if lte IE 8]><script src="js/helpers/excanvas.min.js"></script><![endif]-->

        <!-- Include Jquery library from Google's CDN but if something goes wrong get Jquery from local file (Remove 'http:' if you have SSL) -->
        <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
        <script>!window.jQuery && document.write(decodeURI('%3Cscript src="js/vendor/jquery-1.11.1.min.js"%3E%3C/script%3E'));</script>

        <!-- Bootstrap.js -->
        <script src="js/vendor/bootstrap.min.js"></script>

        <!-- Jquery plugins and custom javascript code -->
        <script src="js/plugins.js"></script>
        <script src="js/main.js"></script>

        <!-- Javascript code only for this page -->
        <script>
            $(function () {

                // Hold our table to a variable
                var exampleDatatable = $('#example-editable-datatables');

                /*
                 * Function for handing the data after a cell has been edited
                 *
                 * From here you can send the data with ajax (for example) to handle in your backend
                 *
                 */
                var reqHandle = function (value, settings) {

                    // this, the edited td element
                    console.log(this);

                    // $(this).attr('id'), get the id of the edited td
                    console.log($(this).attr('id'));

                    // $(this).parent('tr').attr('id'), get the id of the row
                    console.log($(this).parent('tr').attr('id'));

                    // value, the new value the user submitted
                    console.log(value);

                    // settings, the settings of jEditable
                    console.log(settings);

                    // Here you can send and handle the data in your backend
                    // ...

                    // For this example, just return the data the user submitted
                    return(value);
                };

                /*
                 * Function for initializing jEditable handlers to the table
                 *
                 * For advance usage you can check http://www.appelsiini.net/projects/jeditable
                 *
                 */
                var initEditable = function (rowID) {

                    // Hold the elements that the jEditable will be initialized
                    var elements;

                    // If we don't have a rowID apply to all td elements with .editable-td class
                    if (!rowID)
                        elements = $('td.editable-td', editableTable.fnGetNodes());
                    else
                        elements = $('td.editable-td', editableTable.fnGetNodes(rowID));

                    elements.editable(reqHandle, {
                        "callback": function (sValue, y) {
                            // Little fix for responsive table after edit
                            exampleDatatable.css('width', '100%');
                        },
                        "submitdata": function (value, settings) {
                            // Sent some extra data
                            return {
                                "row_id": this.parentNode.getAttribute('id'),
                                "column": editableTable.fnGetPosition(this)[2]
                            };
                        },
                        indicator: '<i class="fa fa-spinner fa-spin"></i>',
                        cssclass: 'remove-margin',
                        submit: 'Ok',
                        cancel: 'Cancel'
                    });
                };

                /*
                 * Function for deleting table row
                 *
                 */
                var delHandle = function () {

                    // When the user clicks on a delete button
                    $('body').on('click', 'a.delRow', function () {
                        var aPos = editableTable.fnGetPosition(this.parentNode);
                        var aData = editableTable.fnGetData(aPos[0]);
                        var rowID = $(this).parents('tr').attr('id');

                        // Here you can handle the deletion of the row in your backend
                        // ...

                        // Delete row if success with the backend
                        editableTable.fnDeleteRow(aPos[0]);
                    });
                };

                /*
                 * Function for adding table row
                 *
                 */
                var addHandle = function () {

                    // When the user clicks on the 'Add New User' button
                    $("#addRow").click(function () {

                        // Here you can handle your backend data (eg: adding a row to database and return the id of the row)

                        // ..

                        // Create a new row and set it up
                        var rowID = editableTable.fnAddData(['', '', '', '', '']);

                        // Example id, here you should add the one you created in your backend
                        var id = rowID[0] + 1;

                        // Update the id cell, so that our table redraw and resort (new row goes first in datatable)
                        editableTable.fnUpdate(id, rowID[0], 1);

                        // Get the new row
                        var nRow = editableTable.fnGetNodes(rowID[0]);

                        /*
                         * In the following section you should set up your cells
                         */
                        // Add id to tr element
                        $(nRow).attr('id', id);

                        // Setup first cell with the delete button
                        $(nRow)
                            .children('td:nth-child(1)')
                            .addClass('text-center')
                            .html('<a href="javascript:void(0)" id="delRow' + id + '" class="btn btn-xs btn-danger delRow"><i class="fa fa-times"></i></a>');

                        // Setup second cell (id)
                        $(nRow)
                            .children('td:nth-child(2)')
                            .attr('id', 'id' + id)
                            .addClass('text-center');

                        // Setup third cell (username)
                        $(nRow)
                            .children('td:nth-child(3)')
                            .addClass('editable-td')
                            .attr('id', 'username' + id);

                        // Setup fourth cell (email)
                        $(nRow)
                            .children('td:nth-child(4)')
                            .addClass('editable-td')
                            .addClass('hidden-xs hidden-sm')
                            .attr('id', 'email' + id);

                        // Setup fifth cell (notes)
                        $(nRow)
                            .children('td:nth-child(5)')
                            .addClass('editable-td')
                            .addClass('hidden-xs hidden-sm')
                            .attr('id', 'notes' + id);

                        // Setup your other cells the same way (if you have more)
                        // ...

                        // Initialize jEditable to the new row
                        initEditable(rowID[0]);

                        // Little fix for responsive table after adding a new row
                        exampleDatatable.css('width', '100%');
                    });
                };

                // Initialize Datatables
                var editableTable = exampleDatatable.dataTable({
                    order: [[1, 'desc']],
                    columnDefs: [{orderable: false, targets: [0]}]
                });
                $('.dataTables_filter input').attr('placeholder', 'Search');

                // Initialize jEditable
                initEditable();

                // Handle rows deletion
                delHandle();

                // Handle new rows
                addHandle();
            });
        </script>
    </body>
</html>
