<?php
// Include config file
require_once "config/config.php";
// Define variables and initialize with empty values
$username = $password = "";
$username_err = $password_err = "";
// Processing form data when form is submitted
if($_SERVER["REQUEST_METHOD"] == "POST"){
    // Check if username is empty
    if(empty(trim($_POST["username"]))){
        $username_err = 'Please enter username.';
    } else{
        $username = trim($_POST["username"]);
    }
    // Check if password is empty
    if(empty(trim($_POST['password']))){
        $password_err = 'Please enter your password.';
    } else{
        $password = trim($_POST['password']);
    } 
    // Validate credentials
    if(empty($username_err) && empty($password_err)){
        // Prepare a select statement
        $sql = "SELECT * FROM akun WHERE username = '$username' AND password = '$password'";
        $query = mysqli_query($link, $sql);
        while ($row =mysqli_fetch_array($query)){
            $perm = $row['perm'];
            if($stmt = mysqli_prepare($link, $sql)){
                // Bind variables to the prepared statement as parameters
                mysqli_stmt_bind_param($stmt, "s",$param_username);
                // Set parameters
                $param_username = $username;
                // Attempt to execute the prepared statement
                if(mysqli_stmt_execute($stmt)){
                    // Store result
                    mysqli_stmt_store_result($stmt);
                    // Check if username exists, if yes then verify password
                    if(mysqli_stmt_num_rows($stmt) == 1){                    
                        // Bind result variables
                        mysqli_stmt_bind_result($stmt, $username);
                        if(mysqli_stmt_fetch($stmt)){
                            session_start();
                            $_SESSION['username'] = $perm;  
                            header("location: index.php?".$row['perm']."-id=".$row['id_akun']."");
                        }
                    } else{
                        // Display an error message if username doesn't exist
                        $password_err = 'username or password wrong! please try again.';
                    }
                } else{
                    echo "Oops! Something went wrong. Please try again later.";
                }
            }
            // Close statement
            mysqli_stmt_close($stmt);
        }    
    }
    // Close connection
    mysqli_close($link);
}
?>