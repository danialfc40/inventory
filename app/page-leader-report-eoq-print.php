<?php
    require('pdf/fpdf.php');
    class PDF extends FPDF {
        function Header() {
            $this->Image('pdf/logo.png',10,8,33);
        }
        function Footer() {
            $this->SetY(-15);
            $this->SetFont('Arial','I',8);
            $this->Cell(0,10,''.$this->PageNo().'',0,0,'C');
        }
    }
    require_once('config/config.php');
    $sql    = "SELECT * FROM eoq"; 
    $query  = mysqli_query($link, $sql);
    $sql1    = "SELECT * FROM transaksi WHERE jenis_transaksi='MASUK' AND status='DONE'"; 
    $query1  = mysqli_query($link, $sql1);
    $sql2    = "SELECT * FROM transaksi WHERE jenis_transaksi='MASUK' AND status='PROCCED'"; 
    $query2  = mysqli_query($link, $sql2);
    $pdf    = new PDF('L','mm','A4');
    $pdf->AddPage();
    $pdf->AliasNbPages();
    $pdf->SetAutoPageBreak(false);
    $pdf->SetAuthor('INSERT AUTHOR');
    $pdf->SetTitle('INSERT DOC TITLE');
    $pdf->SetFont('Arial','B',14);
    $pdf->Cell(-10,7,'                                                                       LAPORAN ECONOMIC ORDER QUANTITY');
    $pdf->SetFont('Arial','B',10);
    $date = date("F j, Y");
    $pdf->Cell(10,35,'          REPORT DATE : '.$date);
    $pdf->SetFont('Arial','B',10);
    $pdf->setXY(10, 10);
    $pdf->Cell(10,50,'TOTAL TRANSACTION EOQ : '.mysqli_num_rows($query));
    $pdf->SetFont('Arial','B',10);
    $pdf->SetDrawColor(0, 0, 0);
    $pdf->SetFillColor(170, 170, 170);
    $pdf->setFont("Arial","B","9");
    $pdf->setXY(10, 50); 
    $pdf->Cell(8, 10, "NO.", 1, 0, "L ", 1); 
    $pdf->Cell(15, 10, "EOQ ID.", 1, 0, "L", 1);
    $pdf->Cell(30, 10, "GOOD NO.", 1, 0, "L", 1);
    $pdf->Cell(20, 10, "DEMAND", 1, 0, "L", 1);
    $pdf->Cell(25, 10, "COST ORDER", 1, 0, "L", 1); 
    $pdf->Cell(25, 10, "COST UNIT", 1, 0, "L", 1);  
    $pdf->Cell(35, 10, "COST WAREHOUSE", 1, 0, "L", 1);  
    $pdf->Cell(8, 10, "%", 1, 0, "L", 1);  
    $pdf->Cell(16, 10, "SS", 1, 0, "L", 1);  
    $pdf->Cell(13, 10, "LT", 1, 0, "L", 1);  
    $pdf->Cell(20, 10, "EOQ", 1, 0, "L", 1);  
    $pdf->Cell(20, 10, "ROP", 1, 0, "L", 1);  
    $pdf->Cell(26, 10, "FREKUENSI", 1, 0, "L", 1);  
    $pdf->Cell(16, 10, "OP", 1, 0, "L", 1);  
    $y = 60;
    $x = 10;  
    $pdf->setXY($x, $y);
    $pdf->setFont("Arial","","9");
    $no=1; 
    while($row = mysqli_fetch_array($query)) {
            $pdf->Cell(8, 8, $no++, 1);
            $pdf->Cell(15, 8, $row['id_eoq'], 1);
            $pdf->Cell(30, 8, $row['id_barang'], 1);
            $pdf->Cell(20, 8, $row['demand'].' UNIT', 1);
            $pdf->Cell(25, 8, 'RP. '.$row['co'].',-', 1);
            $pdf->Cell(25, 8, 'RP. '.$row['cu'].',-', 1);
            $pdf->Cell(35, 8, 'RP. '.$row['cw'].',-', 1);
            $pdf->Cell(8, 8, $row['persen'].'%', 1);
            $pdf->Cell(16, 8, $row['ss'].' UNIT', 1);
            $pdf->Cell(13, 8, $row['lt'].' DAY', 1);
            $pdf->Cell(20, 8, $row['eoq'].' UNIT', 1);
            $pdf->Cell(20, 8, $row['rop'].' UNIT', 1);
            $pdf->Cell(26, 8, $row['fk'], 1);
            $pdf->Cell(16, 8, $row['op'].' UNIT', 1);
            $y += 8;
            if ($y > 260) {
                $pdf->AddPage();
                $y = 40; 
            }
            $pdf->setXY($x, $y);
    }
    $pdf->Output();
?>