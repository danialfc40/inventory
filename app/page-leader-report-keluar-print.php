<?php
    require('pdf/fpdf.php');
    class PDF extends FPDF {
        function Header() {
            $this->Image('pdf/logo.png',10,8,33);
        }
        function Footer() {
            $this->SetY(-15);
            $this->SetFont('Arial','I',8);
            $this->Cell(0,10,''.$this->PageNo().'',0,0,'C');
        }
    }
    require_once('config/config.php');
    $sql    = "SELECT * FROM transaksi WHERE jenis_transaksi='KELUAR'"; 
    $query  = mysqli_query($link, $sql);
    $sql1    = "SELECT * FROM transaksi WHERE jenis_transaksi='KELUAR' AND status='DONE'"; 
    $query1  = mysqli_query($link, $sql1);
    $sql2    = "SELECT * FROM transaksi WHERE jenis_transaksi='KELUAR' AND status='PROCCED'"; 
    $query2  = mysqli_query($link, $sql2);
    $pdf    = new PDF('P','mm','A4');
    $pdf->AddPage();
    $pdf->AliasNbPages();
    $pdf->SetAutoPageBreak(false);
    $pdf->SetAuthor('INSERT AUTHOR');
    $pdf->SetTitle('INSERT DOC TITLE');
    $pdf->SetFont('Arial','B',14);
    $pdf->Cell(-10,7,'                                         LAPORAN TRANSAKSI KELUAR');
    $pdf->SetFont('Arial','B',10);
    $date = date("F j, Y");
    $pdf->Cell(10,35,'          REPORT DATE : '.$date);
    $pdf->SetFont('Arial','B',10);
    $pdf->setXY(10, 10);
    $pdf->Cell(10,50,'TOTAL TRANSACTION OUT : '.mysqli_num_rows($query));
    $pdf->setXY(10, 10); 
    $pdf->Cell(10,60,'TOTAL TRANSACTION STATUS DONE : '.mysqli_num_rows($query1));
    $pdf->setXY(10, 10); 
    $pdf->Cell(10,70,'TOTAL TRANSACTION STATUS PROCCED : '.mysqli_num_rows($query2));
    $pdf->SetFont('Arial','B',10);
    $pdf->SetDrawColor(0, 0, 0);
    $pdf->SetFillColor(170, 170, 170);
    $pdf->setFont("Arial","B","9");
    $pdf->setXY(10, 50); 
    $pdf->Cell(8, 10, "NO.", 1, 0, "L ", 1); 
    $pdf->Cell(35, 10, "TRANSACTION NO.", 1, 0, "L", 1);
    $pdf->Cell(35, 10, "MRF NO.", 1, 0, "L", 1);
    $pdf->Cell(30, 10, "ID STAFF", 1, 0, "L", 1);
    $pdf->Cell(35, 10, "DATE TRANSACTION", 1, 0, "L", 1); 
    $pdf->Cell(25, 10, "STATUS", 1, 0, "L", 1);  
    $y = 60;
    $x = 10;  
    $pdf->setXY($x, $y);
    $pdf->setFont("Arial","","9");
    $no=1; 
    while($row = mysqli_fetch_array($query)) {
            $pdf->Cell(8, 8, $no++, 1);
            $pdf->Cell(35, 8, $row['id_transaksi'], 1);
            $pdf->Cell(35, 8, $row['id_mrf'], 1);
            $pdf->Cell(30, 8, $row['id_akun'], 1);
            $pdf->Cell(35, 8, $row['tgl_transaksi'], 1);
            $pdf->Cell(25, 8, $row['status'], 1);
            $y += 8;
            if ($y > 260) {
                $pdf->AddPage();
                $y = 40; 
            }
            $pdf->setXY($x, $y);
    }
    $pdf->Output();
?>