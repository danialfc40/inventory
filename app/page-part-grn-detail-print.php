<?php
    require('pdf/fpdf.php');
    class PDF extends FPDF {
        function Header() {
            $this->Image('pdf/logo.png',10,8,33);
        }
        function Footer() {
            $this->SetY(-15);
            $this->SetFont('Arial','I',8);
            $this->Cell(0,10,''.$this->PageNo().'',0,0,'C');
        }
    }
    require_once('config/config.php');
    $id     = $_GET['print']; 
    $sql    = "SELECT * FROM detail_grn WHERE id_grn='$id'"; 
    $sql2   = "SELECT * FROM detail_grn WHERE id_grn='$id'";
    $sql3   = "SELECT * FROM grn WHERE id_grn='$id'"; 
    $query  = mysqli_query($link, $sql);
    $query2 = mysqli_query($link, $sql2);
    $query3 = mysqli_query($link, $sql3);
    $row2   = mysqli_fetch_array($query2);
    $row3   = mysqli_fetch_array($query3);    
    $idd    = $row3['id_do'];
    $ida    = $row3['id_akun'];
    $sql5   = "SELECT * FROM do WHERE id_do='$idd'";
    $query5 = mysqli_query($link, $sql5);
    $sql6   = "SELECT * FROM akun WHERE id_akun='$ida'";
    $query6 = mysqli_query($link, $sql6);
    $row6   = mysqli_fetch_array($query6);
    $pdf    = new PDF('L','mm','A4');
    $pdf->AddPage();
    $pdf->AliasNbPages();
    $pdf->SetAutoPageBreak(false);
    $pdf->SetAuthor('INSERT AUTHOR');
    $pdf->SetTitle('INSERT DOC TITLE');
    $pdf->SetFont('Arial','B',14);
    $pdf->Cell(-10,7,'                                                                             GOODS RECEIPT NOTE');
    $pdf->SetFont('Arial','B',10);
    $date = date("F j, Y");
    $pdf->Cell(10,35,'          RECIVED DATE : '.$date);
    $pdf->SetFont('Arial','B',10);
    $pdf->Cell(10,50,'GRN DOC NO. : '.$id);
    $pdf->SetDrawColor(0, 0, 0);
    $pdf->SetFillColor(170, 170, 170);
    $pdf->setFont("Arial","B","9");
    $pdf->setXY(10, 40); 
    $pdf->Cell(8, 10, "NO.", 1, 0, "L", 1); 
    $pdf->Cell(40, 10, "PART NO.", 1, 0, "L", 1);
    $pdf->Cell(60, 10, "DESCRIPTION", 1, 0, "L", 1);
    $pdf->Cell(35, 10, "UNIT MEASURE", 1, 0, "L", 1);
    $pdf->Cell(10, 10, "QTY", 1, 0, "L", 1); 
    $pdf->Cell(55, 10, "REFERENCE DOC RO NO./POST", 1, 0, "L", 1); 
    $pdf->Cell(55, 10, "REMARKS", 1, 0, "L", 1); 
    $y = 50;
    $x = 10;  
    $pdf->setXY($x, $y);
    $pdf->setFont("Arial","","9");
    $no=1; 
    while($row = mysqli_fetch_array($query)) {
        $asu    = $row['id_barang'];
        $sql4   = "SELECT * FROM barang WHERE id_barang='$asu'";
        $query4 = mysqli_query($link, $sql4);
        $row4   = mysqli_fetch_array($query4);
        $row5   = mysqli_fetch_array($query5);
            $pdf->Cell(8, 8, $no++, 1);
            $pdf->Cell(40, 8, $row['id_barang'], 1);
            $pdf->Cell(60, 8, $row4['description'], 1);
            $pdf->Cell(35, 8, '', 1);
            $pdf->Cell(10, 8, $row['qty'], 1);
            $pdf->Cell(55, 8, $row5['id_ro'].' / '.$row5['id_do'], 1);
            $pdf->Cell(55, 8, '', 1);
            $y += 8;
            if ($y > 260) {
                $pdf->AddPage();
                $y = 40; 
            }
            $pdf->setXY($x, $y);
    }
    $pdf->SetFont('Arial','B',10);
    $pdf->Cell(10,30,'          Recived By');
    $pdf->SetFont('Arial','U',10);
    $pdf->Cell(10,80, $row6['nama_akun']);
    $pdf->Output();
?>