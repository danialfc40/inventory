<?php 
    require 'config/config.php';
    if (!$link) {
        die ('Failed to connect to MySQL: ' . mysqli_connect_error());  
    }
    $sql = 'SELECT * FROM barang';  
    $sql2 = 'SELECT * FROM detail_barang';
    $query = mysqli_query($link, $sql);
    $query2 = mysqli_query($link, $sql2);
    if (!$query && !$query2) {
        die ('SQL Error: ' . mysqli_error($conn));
    }
?>
<!-- Page Content -->
<div id="page-content">
    <!-- Navigation info -->
    <ul id="nav-info" class="clearfix">
        <li><a href="index.php"><i class="fa fa-home"></i></a></li>
        <li><a href="javascript:void(0)">Transaksi</a></li>
        <li class="active"><a href="">Stok Barang</a></li>
    </ul>
    <!-- END Navigation info -->

    <!-- Editable Datatables -->
    <h3 class="page-header page-header-top">Data Table <small>Stok Barang.</small></h3>
    <div class="dash-tiles row">
            <!-- Column 1 of Row 1 -->

        <div class="col-sm-12">
            <div class="push">
                <a href="page-part-stok-tambah.php" class="btn btn-success"><i class="fa fa-plus"></i> Add Barang</a>
            </div>
            <!-- Table -->
            <table id="example-editable-datatables" class="table table-bordered table-hover">
                <thead>
                    <tr>
                        <th class="cell-small text-center">No.</th>
                        <th><i class="fa fa-user"></i> ID Barang</th>
                        <th class="hidden-xs hidden-sm"><i class="fa fa-envelope-o"></i> Nama Barang</th>
                        <th class="hidden-xs hidden-sm"><i class="fa fa-file"></i> Description</th>
                        <th class="cell-small"></th>
                        <th class="hidden-xs hidden-sm"><i class="fa fa-file"></i> Qty</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                        $no = 1;
                        while ($row = mysqli_fetch_array($query)) {
                            $row2 = mysqli_fetch_array($query2);
                            echo '<tr>
                                    <td>'.$no++.'</td>
                                    <td>'.$row['id_barang'].'</td>
                                    <td>'.$row['nama_barang'].'</td>
                                    <td>'.$row['description'].'</td>
                                    <td><a href="page-part-stok-edit.php?edit='.$row['id_barang'].'" class= "btn btn-xs btn-success"><i class="fa fa-pencil"></i></a> <a href="page-part-stok-delete.php?delete='.$row['id_barang'].'" class= "btn btn-xs btn-danger" ><i class="fa fa-times-circle"></i></a></td>
                                    <td>'.$row2['qty'].'</td>
                                </tr>';
                        }
                    ?>
                </tbody>
            </table>
            <!-- END Table -->
            <!-- END Editable Datatables -->
            <!-- Javascript code only for this page -->
        </div>
    </div>
</div>
<!-- END Page Content -->
<?php include 'footer.php'; ?>

