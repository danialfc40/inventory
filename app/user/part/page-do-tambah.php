                <div id="page-content">
                    <ul id="nav-info" class="clearfix">
                        <li><a href="index.php"><i class="fa fa-home"></i></a></li>
                        <li><a href="javascript:void(0)">Transaksi</a></li>
                        <li><a href="javascript:void(0)">Request Stok</a></li>
                        <li class="active"><a href="">Tambah DO</a></li>
                    </ul>
                    <form action="page-part-do-tambah-proses.php" method="POST" class="form-horizontal form-box">
                        <h4 class="form-box-header">Tambah DO</h4>
                        <div class="form-box-content col-sm-4">
                            <div class="form-group">
                                <label class="control-label col-md-2">DO Job Number</label>
                                <div class="col-md-4">
                                    <?php 
                                        require 'config.php';
                                        if (!$link) {
                                            die ('Failed to connect to MySQL: ' . mysqli_connect_error());  
                                        }  
                                        $sql = 'SELECT * FROM do'; 
                                        $sql2 = 'SELECT * FROM ro WHERE satus="REQUESTED"';
                                        $sql3 = 'SELECT * FROM po WHERE status="PROCCED"';
                                        $carikode = mysqli_query($link, "SELECT max(id_do) FROM do");
                                        $datakode = mysqli_fetch_array($carikode);
                                        $query = mysqli_query($link, $sql);
                                        $query2 = mysqli_query($link, $sql2);
                                        $query3 = mysqli_query($link, $sql3);
                                        if (!$query) {
                                            die ('SQL Error: ' . mysqli_error($conn));
                                        }
                                        if ($datakode) {
                                           $nilaikode = substr($datakode[0], 2);
                                           // menjadikan $nilaikode ( int )
                                           $kode = (int) $nilaikode;
                                           // setiap $kode di tambah 1
                                           $kode1 = $kode + 1;
                                           $kode_otomatis = "DO".str_pad($kode1, 3, "0", STR_PAD_LEFT);
                                          } else {
                                           $kode_otomatis = "DO001";
                                        }
                                        echo '<input type="text" id="id_do" name="id_do" class="form-control" value="'.$kode_otomatis.'"/>';
                                    ?> 
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-2">RO Job Number</label>
                                <div class="col-md-4">
                                    <select class="form-control" type="text" id="id_ro" name="id_ro">
                                        <?php 
                                            while ($row2 = $query2->fetch_assoc()){
                                                echo '<option value="'.$row2['id_ro'].'">'.$row2['id_ro'].'</option>';
                                            }
                                        ?>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-2">PO Job Number</label>
                                <div class="col-md-4">
                                    <select class="form-control" type="text" id="id_po" name="id_po">
                                        <?php 
                                            while ($row3 = $query3->fetch_assoc()){
                                                echo '<option value="'.$row3['id_po'].'">'.$row3['id_po'].'</option>';
                                            } 
                                        ?>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-2">Transaction Date</label>
                                <div class="col-md-4">
                                    <input type="date" class="form-control" name="trans">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-2">Jenis Transaksi DO</label>
                                <div class="col-md-4">
                                    <select class="form-control" type="text" id="jenis" name="jenis">
                                        <option value="KELUAR">KELUAR</option>
                                        <option value="MASUK">MASUK</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-2" >Status</label>
                                <div class="col-md-4">
                                    <input type="hidden" class="form-control" name="stat" value="PROCCED">
                                    <input type="text" class="form-control" name="stat" value="PROCCED" disabled>
                                </div>
                            </div>
                            <div class="form-group form-actions">
                                <div class="col-md-10 col-md-offset-2">
                                    <button class="btn btn-success" type="submit" name="simpan"><i class="fa fa-floppy-o"></i> Save</button>
                                    <a href="page-part-do.php" class="btn btn-danger"><i class="fa fa-times"></i> Cancel</a>
                                </div>
                            </div>
                        </div>
                        <div class="form-box-content col-sm-8">
                            <input type="button" name="add_btn" value="Add" class="btn btn-success" id="add_btn">
                            <table class="table table-bordered table-hover">
                                <thead>
                                    <tr>
                                        <th class="cell-small text-center">No.</th>
                                        <th><i class="fa fa-user"></i> RO Number</th>
                                        <th class="hidden-xs hidden-sm"><i class="fa fa-envelope-o"></i> ID Barang</th>
                                        <th class="hidden-xs hidden-sm"><i class="fa fa-file"></i> Qty</th>
                                        <th class="cell-small"></th>
                                    </tr>
                                </thead>
                                <tbody id="target">
                                </tbody>
                            </table>                            
                        </div>
                    </form>
                </div>
<?php include 'footer.php'; ?>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script type="text/javascript">
    $(document).ready(function() {
            var count = 0;
            var isi = document.getElementById("id_do").value;
            $("#add_btn").click(function(){
                    count += 1;
                $('#target').append(
                             '<tr class="records">'
                         + '<td ><div id="'+count+'">' + count + '</div></td>'
                         + '<td><input id="idi_do_' + count + '" name="idi_do_' + count + '" type="text" value="' + isi + '"></td>'
                         + '<td><input id="idi_barang_' + count + '" name="idi_barang_' + count + '" type="text"></td>'
                         + '<td><input id="qty_' + count + '" name="qty_' + count + '" type="text"></td>'
                         + '<td><a class="remove_item" href="#" >Delete</a>'
                         + '<input id="rows_' + count + '" name="rows[]" value="'+ count +'" type="hidden"></td></tr>'
                    );
                });
                $("#target").on('click', '.remove_item', function() {
                    $(this).parent().parent().remove();
                });
        });
</script>