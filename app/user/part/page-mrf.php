<?php 
    require 'config/config.php';
    if (!$link) {
        die ('Failed to connect to MySQL: ' . mysqli_connect_error());  
    }
    $sql = 'SELECT * FROM mrf';  
    $query = mysqli_query($link, $sql);
    if (!$query) {
        die ('SQL Error: ' . mysqli_error($conn));
    }
?>
<!-- Page Content -->
<div id="page-content">
    <!-- Navigation info -->
    <ul id="nav-info" class="clearfix">
        <li><a href="index.php"><i class="fa fa-home"></i></a></li>
        <li><a href="javascript:void(0)">Transaksi</a></li>
        <li class="active"><a href="">MRF</a></li>
    </ul>
    <!-- END Navigation info -->

    <!-- Editable Datatables -->
    <h3 class="page-header page-header-top">Data Table <small>Stok Barang.</small></h3>
    <div class="dash-tiles row">
            <!-- Column 1 of Row 1 -->

        <div class="col-sm-12">
            <div class="push">
                <?php 
                    $sql1 = 'SELECT * FROM do WHERE jenis_do="KELUAR" AND status="PROCCED"';
                    $query1 = mysqli_query($link, $sql1);
                    $row1 = mysqli_num_rows($query1);
                    if ($row1 > 0){
                        echo '<a href="page-part-mrf-tambah.php" class="btn btn-success"><i class="fa fa-plus"></i> Add MRF</a>';
                    } else if ($row1 <= 0) {
                        echo '<a href="page-part-mrf-tambah.php" class="btn btn-success" Disabled><i class="fa fa-plus"></i> Add MRF</a>';
                    }
                ?>
            </div>
            <!-- Table -->
            <table id="example-editable-datatables" class="table table-bordered table-hover">
                <thead>
                    <tr>
                        <th class="cell-small text-center">No.</th>
                        <th><i class="fa fa-user"></i> MRF Number</th>
                        <th><i class="fa fa-user"></i> DO Number</th>
                        <th><i class="fa fa-user"></i> Staff Number</th>
                        <th class="hidden-xs hidden-sm"><i class="fa fa-envelope-o"></i> Date</th>
                        <th class="hidden-xs hidden-sm"><i class="fa fa-file"></i> Status</th>
                        <th class="cell-small"></th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                        $no = 1;
                        while ($row = mysqli_fetch_array($query)) {
                            echo '<tr>
                                    <td>'.$no++.'</td>
                                    <td>'.$row['id_mrf'].'</td>
                                    <td>'.$row['id_do'].'</td>
                                    <td>'.$row['id_akun'].'</td>
                                    <td>'.$row['tgl_mrf'].'</td>
                                    <td>'.$row['status'].'</td>
                                    <td><a href="page-part-mrf-detail.php?detail='.$row['id_mrf'].'" class= "btn btn-xs btn-success"><i class="fa fa-bell-o"></i></a> <a href="page-part-mrf-delete.php?delete='.$row['id_mrf'].'" class= "btn btn-xs btn-danger"><i class="fa fa-times-circle"></i></a></td>
                                </tr>';
                        }
                    ?>
                </tbody>
            </table>
        </div>
    </div>
</div>
<!-- END Page Content -->
<?php include 'footer.php'; ?>

