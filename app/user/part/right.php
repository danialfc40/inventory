<!-- Page Content -->
                <div id="page-content">
                    <!-- Navigation info -->
                    <ul id="nav-info" class="clearfix">
                        <li><a href="index.php"><i class="fa fa-home"></i></a></li>
                        <li class="active"><a href="">Dashboard</a></li>
                    </ul>
                    <!-- END Navigation info -->
                    <!-- Nav Dash -->
                    <ul class="nav-dash">
                        <li>
                            <h1>Welcome to Trane Inventory Management System</h1>
                        </li>
                    </ul>  
                    <?php 
                        require 'config/config.php';
                        $query  = mysqli_query($link, "SELECT * FROM barang");
                        $query2 = mysqli_query($link, "SELECT * FROM transaksi");
                        $query3 = mysqli_query($link, "SELECT * FROM transaksi WHERE status='PROCCED'");
                        $query4 = mysqli_query($link, "SELECT * FROM customer");
                        $query5 = mysqli_query($link, "SELECT * FROM mrf");
                        $query6 = mysqli_query($link, "SELECT * FROM mrf WHERE status='PROCCED'");
                        $query7 = mysqli_query($link, "SELECT * FROM grn");
                        $query8 = mysqli_query($link, "SELECT * FROM grn WHERE status='PROCCED'");
                        $query9 = mysqli_query($link, "SELECT * FROM po");
                        $query10= mysqli_query($link, "SELECT * FROM po WHERE status='PROCCED'");
                        $query11= mysqli_query($link, "SELECT * FROM do");
                        $query12= mysqli_query($link, "SELECT * FROM do WHERE status='PROCCED'");
                        $query13= mysqli_query($link, "SELECT * FROM ro");
                        $query14= mysqli_query($link, "SELECT * FROM ro WHERE satus='REQUESTED'");
                    ?>
                    <div class="dash-tiles row">
                        <!-- Column 1 of Row 1 -->
                        <div class="col-sm-3">
                            <!-- Total Users Tile -->
                            <div class="dash-tile dash-tile-ocean clearfix animation-pullDown">
                                <div class="dash-tile-header">
                                    Total Stok
                                </div>
                                <div class="dash-tile-icon"><i class="fa fa-barcode"></i></div>
                                <div class="dash-tile-text"><?php echo mysqli_num_rows($query);?> Unit</div>
                            </div>
                            <div class="dash-tile dash-tile-leaf clearfix animation-pullDown">
                                <div class="dash-tile-header">
                                    Total Customer
                                </div>
                                <div class="dash-tile-icon"><i class="fa fa-users"></i></div>
                                <div class="dash-tile-text"><?php echo mysqli_num_rows($query4);?> <p>Customer</p></div>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="dash-tile dash-tile-flower clearfix animation-pullDown">
                                <div class="dash-tile-header">
                                    Total Transaction : <?php echo mysqli_num_rows($query2);?>
                                </div>
                                <div class="dash-tile-icon"><i class="fa fa-tags"></i></div>
                                <div class="dash-tile-text"><h3><?php echo mysqli_num_rows($query3);?> Not Done</h3></div>
                            </div>
                            <div class="dash-tile dash-tile-fruit clearfix animation-pullDown">
                                <div class="dash-tile-header">
                                    Total PO : <?php echo mysqli_num_rows($query9);?>
                                </div>
                                <div class="dash-tile-icon"><i class="fa fa-cloud-download"></i></div>
                                <div class="dash-tile-text"><h3><?php echo mysqli_num_rows($query10);?> Not Done</h3></div>
                            </div> 
                        </div>
                        <div class="col-sm-3">
                            <div class="dash-tile dash-tile-oil clearfix animation-pullDown">
                                <div class="dash-tile-header">
                                    Total MRF : <?php echo mysqli_num_rows($query5); ?>
                                </div>
                                <div class="dash-tile-icon"><i class="fa fa-globe"></i></div>
                                <div class="dash-tile-text"><?php echo mysqli_num_rows($query6); ?> Not Done</div>
                            </div>
                            <div class="dash-tile dash-tile-dark clearfix animation-pullDown">
                                <div class="dash-tile-header">
                                    Total GRN : <?php echo mysqli_num_rows($query7); ?>
                                </div>
                                <div class="dash-tile-icon"><i class="gi gi-cargo"></i></div>
                                <div class="dash-tile-text"><h3><?php echo mysqli_num_rows($query8); ?> Not Done</h3></div>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="dash-tile dash-tile-balloon clearfix animation-pullDown">
                                <div class="dash-tile-header">
                                    Total DO : <?php echo mysqli_num_rows($query11); ?>
                                </div>
                                <div class="dash-tile-icon"><i class="gi gi-notes_2"></i></div>
                                <div class="dash-tile-text"><h3><?php echo mysqli_num_rows($query12); ?> Not Done</h3></div>
                            </div>
                            <div class="dash-tile dash-tile-doll clearfix animation-pullDown">
                                <div class="dash-tile-header">
                                    Total RO : <?php echo mysqli_num_rows($query13); ?>
                                </div>
                                <div class="dash-tile-icon"><i class="gi gi-file"></i></div>
                                <div class="dash-tile-text"><h3><?php echo mysqli_num_rows($query14); ?> Not Done</h3></div>
                            </div>
                        </div>
                    </div>
                </div>
                <?php include 'footer.php'; ?>