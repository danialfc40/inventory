<?php 
    require 'config/config.php';
    if (!$link) {
        die ('Failed to connect to MySQL: ' . mysqli_connect_error());  
    }
    $sql = 'SELECT * FROM do';  
    $query = mysqli_query($link, $sql);
    if (!$query) {
        die ('SQL Error: ' . mysqli_error($conn));
    }
?>
<!-- Page Content -->
<div id="page-content">
    <!-- Navigation info -->
    <ul id="nav-info" class="clearfix">
        <li><a href="index.php"><i class="fa fa-home"></i></a></li>
        <li><a href="javascript:void(0)">Transaksi</a></li>
        <li class="active"><a href="">Stok Barang</a></li>
    </ul>
    <!-- END Navigation info -->

    <!-- Editable Datatables -->
    <h3 class="page-header page-header-top">Data Table <small>Stok Barang.</small></h3>
    <div class="dash-tiles row">
            <!-- Column 1 of Row 1 -->

        <div class="col-sm-12">
            <div class="push">
                <?php 
                    $sql1 = 'SELECT * FROM ro WHERE satus = "REQUESTED"';
                    $query1 = mysqli_query($link, $sql1);
                    $row1 = mysqli_num_rows($query1);
                    $sql2 = 'SELECT * FROM po WHERE status = "PROCCED"';
                    $query2 = mysqli_query($link, $sql2);
                    $row2 = mysqli_num_rows($query2);
                    if ($row1 > 0){
                        echo '<a href="page-part-do-tambah.php" class="btn btn-success"><i class="fa fa-plus"></i> Add DO Masuk</a>';
                    } else if ($row1 <= 0){
                        echo '<a href="page-part-do-tambah.php" class="btn btn-success"Disabled><i class="fa fa-plus"></i> Add DO Masuk</a>';
                    }
                    if ($row2 > 0){
                        echo ' <a href="page-part-do-tambah.php" class="btn btn-success"><i class="fa fa-plus"></i> Add DO Keluar</a>';
                    } else if ($row2 <= 0){
                        echo ' <a href="page-part-do-tambah.php" class="btn btn-success"Disabled><i class="fa fa-plus"></i> Add DO Keluar</a>';
                    }
                ?>
            </div>
            <!-- Table -->
            <table id="example-editable-datatables" class="table table-bordered table-hover">
                <thead>
                    <tr>
                        <th class="cell-small text-center">No.</th>
                        <th><i class="fa fa-user"></i> DO Number</th>
                        <th><i class="fa fa-user"></i> RO Number</th>
                        <th><i class="fa fa-user"></i> PO Number</th>
                        <th class="hidden-xs hidden-sm"><i class="fa fa-envelope-o"></i> Date</th>
                        <th class="hidden-xs hidden-sm"><i class="fa fa-file"></i> Jenis DO</th>
                        <th class="hidden-xs hidden-sm"><i class="fa fa-file"></i> Status</th>
                        <th class="cell-small"></th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                        $no = 1;
                        while ($row = mysqli_fetch_array($query)) {
                            echo '<tr>
                                    <td>'.$no++.'</td>
                                    <td>'.$row['id_do'].'</td>
                                    <td>'.$row['id_ro'].'</td>
                                    <td>'.$row['id_po'].'</td>
                                    <td>'.$row['tgl_do'].'</td>
                                    <td>'.$row['jenis_do'].'</td>
                                    <td>'.$row['status'].'</td>
                                    <td><a href="page-part-do-detail.php?detail='.$row['id_do'].'" class= "btn btn-xs btn-success"><i class="fa fa-bell-o"></i></a> <a href="page-part-do-delete.php?delete='.$row['id_do'].'" class= "btn btn-xs btn-danger"><i class="fa fa-times-circle"></i></a></td>
                                </tr>';
                        }
                    ?>
                </tbody>
            </table>
        </div>
    </div>
</div>
<!-- END Page Content -->
<?php include 'footer.php'; ?>

