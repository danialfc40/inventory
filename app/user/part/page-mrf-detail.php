<?php 
                        require 'config.php';
                        $id = $_GET['detail']; 
                        $query = mysqli_query($link, "SELECT * FROM detail_mrf WHERE id_mrf='$id'");
                        $query2 = mysqli_query($link, "SELECT * FROM detail_mrf WHERE id_mrf='$id'");
                        $row2 = mysqli_fetch_array($query2);
?>
<!-- Page Content -->
<div id="page-content">
    <!-- Navigation info -->
    <ul id="nav-info" class="clearfix">
        <li><a href="index.php"><i class="fa fa-home"></i></a></li>
        <li><a href="javascript:void(0)">Transaksi</a></li>
        <li class="active"><a href="">Stok Barang</a></li>
    </ul>
    <!-- END Navigation info -->

    <!-- Editable Datatables -->
    <h3 class="page-header page-header-top">Data Table <small>Stok Barang.</small></h3>
    <div class="dash-tiles row">
            <!-- Column 1 of Row 1 -->

        <div class="col-sm-12">
            <div class="push">
                <a href="page-part-mrf.php" class="btn btn-success"><i class="fa fa-mail-reply"></i> Kembali</a>
                <a href="page-part-mrf-detail-print.php?print=<?php echo $row2['id_mrf']; ?>" class="btn btn-success"><i class="gi gi-print"></i> Cetak</a>
            </div>
            <!-- Table -->
            <table id="example-editable-datatables" class="table table-bordered table-hover">
                <thead>
                    <tr>
                        <th class="cell-small text-center">No.</th>
                        <th><i class="fa fa-user"></i> MRF Number</th>
                        <th class="hidden-xs hidden-sm"><i class="fa fa-envelope-o"></i> ID Barang</th>
                        <th class="hidden-xs hidden-sm"><i class="fa fa-file"></i> Qty</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                        $no = 1;
                        while ($row = mysqli_fetch_array($query)) {
                            echo '<tr>
                                    <td>'.$no++.'</td>
                                    <td>'.$row['id_mrf'].'</td>
                                    <td>'.$row['id_barang'].'</td>
                                    <td>'.$row['qty'].'</td>
                                </tr>';
                        }
                    ?>
                </tbody>
            </table>
        </div>
    </div>
</div>
<!-- END Page Content -->
<?php include 'footer.php'; ?>

