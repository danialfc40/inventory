                <div id="page-content">
                    <ul id="nav-info" class="clearfix">
                        <li><a href="index.php"><i class="fa fa-home"></i></a></li>
                        <li><a href="javascript:void(0)">Transaksi</a></li>
                        <li><a href="javascript:void(0)">Stok Barang</a></li>
                        <li class="active"><a href="">Tambah Barang</a></li>
                    </ul>
                    <form action="page-part-stok-tambah-proses.php" method="POST" class="form-horizontal form-box">
                        <h4 class="form-box-header">Tambah Barang</h4>
                        <div class="form-box-content">
                            <div class="form-group">
                                <label class="control-label col-md-2">ID Barang</label>
                                <div class="col-md-2">
                                    <input type="text" class="form-control" name="id_barang">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-2">Nama Barang</label>
                                <div class="col-md-2">
                                    <input type="text" class="form-control" name="nama">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-2" >Description</label>
                                <div class="col-md-2">
                                    <input type="text" class="form-control" name="desc">
                                </div>
                            </div>
                            <div class="form-group form-actions">
                                <div class="col-md-10 col-md-offset-2">
                                    <button class="btn btn-success" type="submit" name="simpan"><i class="fa fa-floppy-o"></i> Save</button>
                                    <a href="page-part-stok.php" class="btn btn-danger"><i class="fa fa-times"></i> Cancel</a>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
<?php include 'footer.php'; ?>