<div id="page-content">
    <ul id="nav-info" class="clearfix">
        <li><a href="index.php"><i class="fa fa-home"></i></a></li>
        <li><a href="javascript:void(0)">Transaksi</a></li>
        <li><a href="javascript:void(0)">Customer</a></li>
        <li class="active"><a href="">Tambah Customer</a></li>
    </ul>
    <form action="page-part-eoq.php" method="POST" class="form-horizontal form-box">
        <h4 class="form-box-header">EOQ Inputs</h4>
        <div class="form-box-content">
            <div class="form-group">
                <div class="col-md-3">
                    <div class="input-group">
                        <span class="input-group-addon">ID EOQ:</span>
                        <?php 
                            require 'config.php';
                            if (!$link) {
                                die ('Failed to connect to MySQL: ' . mysqli_connect_error());  
                            }  
                            $sql = 'SELECT * FROM barang';
                            $carikode = mysqli_query($link, "SELECT max(id_eoq) FROM eoq");
                            $datakode = mysqli_fetch_array($carikode);
                            $query = mysqli_query($link, $sql);
                            if (!$query) {
                                die ('SQL Error: ' . mysqli_error($conn));
                            }
                            if ($datakode) {
                               $nilaikode = substr($datakode[0], 1);
                               // menjadikan $nilaikode ( int )
                               $kode = (int) $nilaikode;
                               // setiap $kode di tambah 1
                               $kode1 = $kode + 1;
                               $kode_otomatis = "E".str_pad($kode1, 3, "0", STR_PAD_LEFT);
                              } else {
                               $kode_otomatis = "E001";
                            }
                            echo '<input type="hidden" name="id_eoq" class="form-control" value="'.$kode_otomatis.'"/>
                                <input type="text" name="id_eoq" class="form-control" value="'.$kode_otomatis.'" disabled/>';
                        ?> 
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="input-group">
                        <span class="input-group-addon">Barang :</span>
                        <select class="form-control" type="text" id="id_barang" name="id_barang">
                            <?php 
                                while ($row = $query->fetch_assoc()){
                                    echo '<option value="'.$row['id_barang'].'">'.$row['id_barang'].'</option>';
                                }
                            ?>
                        </select>
                    </div>
                </div>
                <div class="col-md-3">
                    <?php 
                        if (isset($_POST['proses'])){
                            $ide = $_POST["id_eoq"];
                            $idb = $_POST["id_barang"];
                            $fp2 = $_POST["fp1"];
                            $a  = $_POST["demand"];
                            $b  = $_POST["costorder"];
                            $c  = $_POST["costunit"];
                            $d  = $_POST["persen"];
                            $e  = $_POST["ss"];
                            $f  = $_POST["lt"];
                            $costwarehouse = ($d/100)*$c;
                            $eoq = (2*$a*$b)/$costwarehouse;
                            $eoq1= round(sqrt($eoq));
                            $rop = round((($a/320)*$f)+$e);
                            $fp  = round($a/$eoq1);
                            $fp1 = round($fp);
                            $op  = round($eoq1/$fp);
                            $kali= round(320/$fp);
                            $sql2    = 'INSERT INTO eoq (id_eoq,id_barang,demand,co,cu,cw,persen,ss,lt,eoq,rop,fk,op) VALUES ("'.$ide.'","'.$idb.'","'.$a.'","'.$b.'","'.$c.'","'.$costwarehouse.'","'.$d.'","'.$e.'","'.$f.'","'.$eoq1.'","'.$rop.'","'.$fp1.'x setiap '.$kali.' hari","'.$op.'")';
                            if ($link->query($sql2) === TRUE) {
                            } else {
                                echo "Error: " . $sql . "<br>" . $link->error;
                            }
                        } 
                        if (isset($_POST['refresh'])) {
                            $a  = 0;
                            $b  = 0;
                            $c  = 0;
                            $d  = 0;
                            $e  = 0;
                            $f  = 0;
                            $costwarehouse = 0;
                            $eoq = 0;
                            $eoq1 = 0;
                            $rop = 0;
                            $fp = 0;
                            $fp1 = 0;
                            $op  = 0;
                            $kali= 320/8;
                        }
                    ?>
                </div>
                <div class="col-md-3">
                    <div class="input-group">
                        <span class="input-group-addon">Warehouse Cost/unit :</i></span>
                        <input type="number" name="persen" class="form-control" value="<?php echo $d; ?>">
                        <span class="input-group-addon">%</i></span>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <div class="col-md-3">
                    <div class="input-group">
                        <span class="input-group-addon">Demand :</i></span>
                        <input type="number" name="demand" class="form-control" value="<?php echo $a; ?>">
                        <span class="input-group-addon">Unit/Year</i></span>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="input-group">
                        <span class="input-group-addon">Cost/Order :</span>
                        <span class="input-group-addon">Rp. </span>
                        <input type="Number" name="costorder" class="form-control" value="<?php echo $b; ?>">
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="input-group">
                        <span class="input-group-addon">Price/Unit :</span>
                        <span class="input-group-addon">Rp. </span>
                        <input type="Number" name="costunit" class="form-control" value="<?php echo $c; ?>">
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="input-group">
                        <span class="input-group-addon">Warehouse Cost :</span>
                        <span class="input-group-addon">Rp. </span>
                        <input type="hidden" name="costwarehouse" class="form-control" value="<?php echo $costwarehouse; ?>">
                        <input type="Number" name="costwarehouse" class="form-control" value="<?php echo $costwarehouse; ?>" disabled>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <div class="col-md-3">
                    <div class="input-group">
                        <span class="input-group-addon">Safety Stok :</span>
                        <input type="text" name="ss" class="form-control" value="<?php echo $e; ?>">
                        <span class="input-group-addon">Unit</span>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="input-group">
                        <span class="input-group-addon">Lead Time :</span>
                        <input type="text" name="lt" class="form-control" value="<?php echo $f ?>">
                        <span class="input-group-addon">Day</span>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <div class="col-md-3">
                    <div class="input-group">
                        <span class="input-group-addon">EOQ :</span>
                        <input type="hidden" name="eoq" class="form-control" value="<?php echo $eoq1; ?>">
                        <input type="text" name="eoq" class="form-control" value="<?php echo $eoq1; ?>" disabled>
                        <span class="input-group-addon">Unit/buy</span>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="input-group">
                        <span class="input-group-addon">ROP :</span>
                        <input type="hidden" name="eoq" class="form-control" value="<?php echo $rop; ?>">
                        <input type="text" name="eoq" class="form-control" value="<?php echo $rop; ?>" disabled>
                        <span class="input-group-addon">Unit</span>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="input-group">
                        <span class="input-group-addon">Frekwensi :</span>
                        <input type="hidden" name="fp1" class="form-control" value="<?php echo $fp1; ?>x setiap <?php echo $kali ?> hari">
                        <input type="text" name="fp1" class="form-control" value="<?php echo $fp1; ?>x setiap <?php echo $kali ?> hari" disabled>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="input-group">
                        <span class="input-group-addon">Offset Order Poin :</span>
                        <input type="hidden" name="eoq" class="form-control" value="<?php echo $op; ?>">
                        <input type="text" name="eoq" class="form-control" value="<?php echo $op; ?>" disabled>
                        <span class="input-group-addon">Unit</span>
                    </div>
                </div>
            </div>
            <div class="form-group form-actions">
                <div class="col-md-10 col-md-offset-2">
                    <button class="btn btn-success" type="submit" name="proses"><i class="fa fa-plus"></i> Procced</button>
                    <button class="btn btn-danger" type="submit" name="refresh"><i class="fa fa-times"></i> Refresh</button>
                </div>
            </div>
        </div>
    </form>
     <div class="dash-tiles row">
            <!-- Column 1 of Row 1 -->

        <div class="col-sm-12">
            <table id="example-editable-datatables" class="table table-bordered table-hover">
                <thead>
                    <tr>
                        <th class="cell-small text-center">No.</th>
                        <th>ID EOQ</th>
                        <th>ID Barang</th>
                        <th>Demand</th>
                        <th class="hidden-xs hidden-sm">Cost Order</th>
                        <th class="hidden-xs hidden-sm">Cost Unit</th>
                        <th class="hidden-xs hidden-sm"> Warehouse Cost</th>
                        <th class="hidden-xs hidden-sm"> persen</th>
                        <th>Safety Stok</th>
                        <th>Lead Time</th>
                        <th>EOQ</th>
                        <th class="hidden-xs hidden-sm">ROP</th>
                        <th class="hidden-xs hidden-sm">Frekwensi</th>
                        <th class="hidden-xs hidden-sm">Order Poin</th>
                        <th class="cell-small"></th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                        $no = 1;
                        $sql3 = 'SELECT * FROM eoq';
                        $query3 =mysqli_query($link, $sql3);
                        while ($row3 = mysqli_fetch_array($query3)) {
                            echo '<tr>
                                    <td>'.$no++.'</td>
                                    <td>'.$row3['id_eoq'].'</td>
                                    <td>'.$row3['id_barang'].'</td>
                                    <td>'.$row3['demand'].'</td>
                                    <td>'.$row3['co'].'</td>
                                    <td>'.$row3['cu'].'</td>
                                    <td>'.$row3['cw'].'</td>
                                    <td>'.$row3['persen'].'</td>
                                    <td>'.$row3['ss'].'</td>
                                    <td>'.$row3['lt'].'</td>
                                    <td>'.$row3['eoq'].'</td>
                                    <td>'.$row3['rop'].'</td>
                                    <td>'.$row3['fk'].'</td>
                                    <td>'.$row3['op'].'</td>
                                    <td><a href="page-part-eoq-delete.php?delete='.$row3['id_eoq'].'" class= "btn btn-xs btn-danger"><i class="fa fa-times-circle"></i></a></td>
                                </tr>';
                        }
                    ?>
                </tbody>
            </table>
        </div>
    </div>
</div>

<?php include 'footer.php'; ?>