<?php
/**
 * config.php
 *
 * Author: pixelcave
 *
 * Configuration php file. It contains variables used in the template
 *
 */

// Template variables
$template = array(
    'active_page' => basename($_SERVER['PHP_SELF'])
);

// Primary navigation array (the primary navigation will be created automatically based on this array)
$primary_nav = array(
    array(
        'name'  => 'Dashboard',
        'url'   => 'index.php',
        'icon'  => 'fa fa-fire'
    ),
    array(
        'name'  => 'Transaksi',
        'icon'  => 'fa fa-th-list',
        'sub'   => array(
            array(
                'name'  => 'Warehouse Transaction',
                'url'   => 'page-part-culi.php',
                'icon'  => 'fa fa-file-text'
            ),
            array(
                'name'  => 'Stok Barang',
                'url'   => 'page-part-stok.php',
                'icon'  => 'fa fa-exclamation-triangle'
            ),
            array(
                'name'  => 'GRN',
                'url'   => 'page-part-grn.php',
                'icon'  => 'fa fa-magic'
            ),
            array(
                'name'  => 'MRF',
                'url'   => 'page-part-mrf.php',
                'icon'  => 'fa fa-magic'
            ),
            array(
                'name'  => 'DO',
                'url'   => 'page-part-do.php',
                'icon'  => 'fa fa-flask'
            ),
            array(
                'name'  => 'PO',
                'url'   => 'page-part-po.php',
                'icon'  => 'fa fa-flask'
            ),
            array(
                'name'  => 'Customer',
                'url'   => 'page-part-customer.php',
                'icon'  => 'fa fa-flask'
            )
        )
    ),
    array(
        'name'  => 'Request Stok',
        'icon'  => 'fa fa-table',
        'sub'   => array(
            array(
                'name'  => 'EOQ',
                'url'   => 'page-part-eoq.php',
                'icon'  => 'fa fa-tint'
            ),
            array(
                'name'  => 'Request Order',
                'url'   => 'page-part-ro.php',
                'icon'  => 'fa fa-th'
            )
        )
    )
);