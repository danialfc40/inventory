<?php
/**
 * config.php
 *
 * Author: pixelcave
 *
 * Configuration php file. It contains variables used in the template
 *
 */

// Template variables
$template = array(
    'active_page' => basename($_SERVER['PHP_SELF'])
);

// Primary navigation array (the primary navigation will be created automatically based on this array)
$primary_nav = array(
    array(
        'name'  => 'Dashboard',
        'url'   => 'index.php',
        'icon'  => 'fa fa-fire'
    ),
    array(
        'name'  => 'Transaksi',
        'icon'  => 'fa fa-th-list',
        'sub'   => array(
            array(
                'name'  => 'Warehouse Transaction',
                'url'   => 'page-warehouse-culi.php',
                'icon'  => 'fa fa-file-text'
            ),
            array(
                'name'  => 'Stok Barang',
                'url'   => 'page-warehouse-stok.php',
                'icon'  => 'fa fa-exclamation-triangle'
            ),
        ),
    ),
);