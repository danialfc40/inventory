<?php 
    require 'config/config.php';
    if (!$link) {
        die ('Failed to connect to MySQL: ' . mysqli_connect_error());  
    }
    $sql = 'SELECT * FROM transaksi';  
    $query = mysqli_query($link, $sql);
    $sql1 = 'SELECT * FROM grn WHERE status="PROCCED"';
    $query1 = mysqli_query($link, $sql1);
    $row1 = mysqli_num_rows($query1);
    $sql2 = 'SELECT * FROM mrf WHERE status="PROCCED"';
    $query2 = mysqli_query($link, $sql2);
    $row2 = mysqli_num_rows($query2);
    if (!$query) {
        die ('SQL Error: ' . mysqli_error($conn));
    }
?>
<!-- Page Content -->
<div id="page-content">
    <!-- Navigation info -->
    <ul id="nav-info" class="clearfix">
        <li><a href="index.php"><i class="fa fa-home"></i></a></li>
        <li><a href="javascript:void(0)">Transaksi</a></li>
        <li class="active"><a href="">Transaksi Gudang</a></li>
    </ul>
    <!-- END Navigation info -->
    <!-- Editable Datatables -->
    <h3 class="page-header page-header-top">Data Table <small>Transaksi Gudang.</small></h3>
    <div class="dash-tiles row">
            <!-- Column 1 of Row 1 -->

        <div class="col-sm-12">
            <div class="push">
                <?php 
                    if ($row1 > 0){
                        echo '<a href="page-warehouse-culi-tambah.php" class="btn btn-success"><i class="fa fa-plus"></i> Add Transaksi Masuk</a>';
                    } else if ($row1 <= 0){
                        echo '<a href="page-warehouse-culi-tambah.php" class="btn btn-success" Disabled><i class="fa fa-plus"></i> Add Transaksi Masuk</a>';
                    } if ($row2 > 0){
                        echo ' <a href="page-warehouse-culi-tambah.php" class="btn btn-success"><i class="fa fa-plus"></i> Add Transaksi Keluar</a>';
                    } else if ($row2 <= 0){
                        echo ' <a href="page-warehouse-culi-tambah.php" class="btn btn-success" Disabled><i class="fa fa-plus"></i> Add Transaksi Keluar</a>';
                    }
                ?>
            </div>
            <!-- Table -->
            <table id="example-editable-datatables" class="table table-bordered table-hover">
                <thead>
                    <tr>
                        <th class="cell-small text-center">No.</th>
                        <th><i class="fa fa-user"></i> Transaction Job Number</th>
                        <th><i class="fa fa-user"></i> ID GRN</th>
                        <th><i class="fa fa-user"></i> ID MRF</th>
                        <th class="hidden-xs hidden-sm"><i class="fa fa-envelope-o"></i> ID Akun</th>
                        <th class="hidden-xs hidden-sm"><i class="fa fa-file"></i> Date Transaction</th>
                        <th class="hidden-xs hidden-sm"><i class="fa fa-file"></i> Jenis Transaction</th>
                        <th class="hidden-xs hidden-sm"><i class="fa fa-file"></i> Transaction Status</th>
                        <th class="cell-small"></th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                        $no = 1;
                        while ($row = mysqli_fetch_array($query)) {
                            echo '<tr>
                                    <td>'.$no++.'</td>
                                    <td>'.$row['id_transaksi'].'</td>
                                    <td>'.$row['id_grn'].'</td>
                                    <td>'.$row['id_mrf'].'</td>
                                    <td>'.$row['id_akun'].'</td>
                                    <td>'.$row['tgl_transaksi'].'</td>
                                    <td>'.$row['jenis_transaksi'].'</td>
                                    <td>'.$row['status'].'</td>
                                    <td><a href="page-warehouse-culi-detail.php?detail='.$row['id_transaksi'].'" class= "btn btn-xs btn-success"><i class="fa fa-bell-o"></i></a> <a href="page-warehouse-culi-delete.php?delete='.$row['id_transaksi'].'" class= "btn btn-xs btn-danger"><i class="fa fa-times-circle"></i></a></td>
                                </tr>';
                        }
                    ?>
                </tbody>
            </table>
        </div>
    </div>
</div>
<!-- END Page Content -->
<?php include 'footer.php'; ?>

