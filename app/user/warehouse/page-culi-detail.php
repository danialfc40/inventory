<!-- Page Content -->
<div id="page-content">
    <!-- Navigation info -->
    <ul id="nav-info" class="clearfix">
        <li><a href="index.php"><i class="fa fa-home"></i></a></li>
        <li><a href="javascript:void(0)">Transaksi</a></li>
        <li class="active"><a href="">Stok Barang</a></li>
    </ul>
    
    <!-- END Navigation info -->
    <?php 
                        require 'config/config.php';
                        $id = $_GET['detail']; 
                        $query = mysqli_query($link, "SELECT * FROM transaksi WHERE id_transaksi='$id'");
                        $row = mysqli_fetch_array($query);
                        $a = $row['id_grn'];
                        $b = $row['id_mrf'];
                        $c = $row['jenis_transaksi'];
                        if ($c == "MASUK"){
                            $isi = "page-warehouse-culi-stok-tambah-proses.php";
                            $judul = "GRN Number";
                        }
                        if ($c == "KELUAR"){
                            $isi = "page-warehouse-culi-stok-kurang-proses.php";
                            $judul = "MRF Number";
                        }
    ?>
    <form action="<?php echo $isi; ?>" method="POST" class="form-horizontal form-box">
        <h4 class="form-box-header">Tambah Transaksi</h4>
        
                        <div class="form-box-content col-sm-4">
                            <div class="form-group">
                                <div class="push">
                                    <a href="page-warehouse-culi.php" class="btn btn-danger"><i class="fa fa-mail-reply"></i> Kembali</a>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-2">Transaksi Number</label>
                                <div class="col-md-4">
                                    <input type="hidden" class="form-control" id="id_transaksi" name="id_transaksi" value="<?php echo $row['id_transaksi']; ?>">
                                    <input type="text" class="form-control" id="id_transaksi" name="id_transaksi" value="<?php echo $row['id_transaksi']; ?>" disabled>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-2">GRN Job Number</label>
                                <div class="col-md-4">
                                    <input type="text" class="form-control" name="id_grn" value="<?php echo $a; ?>" disabled>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-2">MRF Job Number</label>
                                <div class="col-md-4">
                                    <input type="text" class="form-control" name="id_mrf" value="<?php echo $b; ?>" disabled>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-2">Staff Number</label>
                                <div class="col-md-4">
                                    <input type="text" class="form-control" name="id_akun" value="<?php echo $row['id_akun']; ?>" disabled>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-2">Transaction Date</label>
                                <div class="col-md-4">
                                    <input type="date" class="form-control" name="trans" value="<?php echo $row['tgl_transaksi']; ?>" disabled> 
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-2">Jenis Transaksi DO</label>
                                <div class="col-md-4">
                                    <input type="text" class="form-control" name="jenis_transaksi" value="<?php echo $c; ?>" disabled>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-2" >Status</label>
                                <div class="col-md-4">
                                    <input type="text" class="form-control" name="status" value="<?php echo $row['status']; ?>" disabled>
                                </div>
                            </div>
                            <div class="form-group form-actions">
                                <div class="col-md-10 col-md-offset-2">
                                    <button class="btn btn-success" type="submit" name="proses"><i class="fa fa-floppy-o"></i> Save</button>
                                </div>
                            </div>
                        </div>
                        <div class="dash-tiles row">
                            <!-- Column 1 of Row 1 -->
                        <div class="col-sm-7">
                            <table id="example-editable-datatables" class="table table-bordered table-hover">
                                <thead>
                                    <tr>
                                        <th class="cell-small text-center">No.</th>
                                        <th><i class="fa fa-user"></i> <?php echo $judul; ?></th>
                                        <th class="hidden-xs hidden-sm"><i class="fa fa-envelope-o"></i> ID Barang</th>
                                        <th class="hidden-xs hidden-sm"><i class="fa fa-file"></i> Qty</th>
                                    </tr>
                                </thead>
                                <tbody>
                                        <?php 
                                            $no = 1;
                                            if ($c == "MASUK"){
                                                $query2 = mysqli_query($link, "SELECT * FROM detail_grn WHERE id_grn='$a'");
                                                while ($row2 = mysqli_fetch_array($query2)) {
                                                echo '<tr>
                                                    <td>'.$no++.'</td>
                                                    <td>'.$row2['id_grn'].'</td>
                                                    <td>'.$row2['id_barang'].'</td>
                                                    <td>'.$row2['qty'].'</td>
                                                </tr>';   
                                                }
                                            } 
                                            if ($c == "KELUAR"){
                                                $query2 = mysqli_query($link, "SELECT * FROM detail_mrf WHERE id_mrf='$b'");
                                                while ($row2 = mysqli_fetch_array($query2)) {
                                                echo '<tr>
                                                    <td>'.$no++.'</td>
                                                    <td>'.$row2['id_mrf'].'</td>
                                                    <td>'.$row2['id_barang'].'</td>
                                                    <td>'.$row2['qty'].'</td>
                                                </tr>';   
                                                }
                                            } 
                                        ?>
                                </tbody>
                            </table>
                        </div>
                        <div class="form-box-content col-sm-7">
                            <input type="button" name="add_btn" value="Add" class="btn btn-success" id="add_btn">
                            <table class="table table-bordered table-hover">
                                <thead>
                                    <tr>
                                        <th class="cell-small text-center">No.</th>
                                        <th class="hidden-xs hidden-sm"><i class="fa fa-envelope-o"></i> ID Barang</th>
                                        <th class="hidden-xs hidden-sm"><i class="fa fa-file"></i> Qty</th>
                                        <th class="cell-small"></th>
                                    </tr>
                                </thead>
                                <tbody id="target">
                                </tbody>
                            </table>                            
                        </div>
                    </div>
                    </form>
    <!-- Editable Datatables -->
    
</div>
<!-- END Page Content -->
<?php include 'footer.php'; ?>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script type="text/javascript">
    $(document).ready(function() {
            var count = 0;
            $("#add_btn").click(function(){
                    count += 1;
                $('#target').append(
                             '<tr class="records">'
                         + '<td ><div id="'+count+'">' + count + '</div></td>'
                         + '<td><input id="idi_barang_' + count + '" name="idi_barang_' + count + '" type="text"></td>'
                         + '<td><input id="qty_' + count + '" name="qty_' + count + '" type="text"></td>'
                         + '<td><a class="remove_item" href="#" >Delete</a>'
                         + '<input id="rows_' + count + '" name="rows[]" value="'+ count +'" type="hidden"></td></tr>'
                    );
                });
                $("#target").on('click', '.remove_item', function() {
                    $(this).parent().parent().remove();
                });
        });
</script>

