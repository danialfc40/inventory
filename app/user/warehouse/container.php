            <!-- Inner Container -->
            <?php include 'page-config.php'; ?>
            <div id="inner-container">
                <!-- Sidebar -->
                <aside id="page-sidebar" class="collapse navbar-collapse navbar-main-collapse">
                    <!-- Sidebar search -->
                    <form id="sidebar-search" action="page_search_results.html" method="post">
                        <div class="input-group">
                            <input type="text" id="sidebar-search-term" name="sidebar-search-term" placeholder="Search..">
                            <button><i class="fa fa-search"></i></button>
                        </div>
                    </form>
                    <!-- END Sidebar search -->
                    <?php include 'left.php'; ?>
                </aside>
                <!-- END Sidebar -->
                <?php include 'right.php'; ?>
                <?php include 'footer.php'; ?>
            </div>
            <!-- END Inner Container -->