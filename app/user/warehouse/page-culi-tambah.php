                <div id="page-content">
                    <ul id="nav-info" class="clearfix">
                        <li><a href="index.php"><i class="fa fa-home"></i></a></li>
                        <li><a href="javascript:void(0)">Transaksi</a></li>
                        <li><a href="javascript:void(0)">Request Stok</a></li>
                        <li class="active"><a href="">Tambah Transaksi</a></li>
                    </ul>
                    <form action="page-warehouse-culi-tambah-proses.php" method="POST" class="form-horizontal form-box">
                        <h4 class="form-box-header">Tambah Transaksi</h4>
                        <div class="form-box-content col-sm-4">
                            <div class="form-group">
                                <label class="control-label col-md-2">Transaksi Number</label>
                                <div class="col-md-4">
                                    <?php 
                                        require 'config/config.php';
                                        if (!$link) {
                                            die ('Failed to connect to MySQL: ' . mysqli_connect_error());  
                                        }  
                                        $sql = 'SELECT * FROM akun'; 
                                        $sql2 = 'SELECT * FROM grn WHERE status="PROCCED"';
                                        $sql3 = 'SELECT * FROM mrf WHERE status="PROCCED"';
                                        $sql4 = 'SELECT * FROM akun WHERE perm="warehose man"';
                                        $carikode = mysqli_query($link, "SELECT max(id_transaksi) FROM transaksi");
                                        $datakode = mysqli_fetch_array($carikode);
                                        $query  = mysqli_query($link, $sql);
                                        $query2 = mysqli_query($link, $sql2);
                                        $query3 = mysqli_query($link, $sql3);
                                        $query4 = mysqli_query($link, $sql4);
                                        if (!$query) {
                                            die ('SQL Error: ' . mysqli_error($conn));
                                        }
                                        if ($datakode) {
                                           $nilaikode = substr($datakode[0], 2);
                                           // menjadikan $nilaikode ( int )
                                           $kode = (int) $nilaikode;
                                           // setiap $kode di tambah 1
                                           $kode1 = $kode + 1;
                                           $kode_otomatis = "T".str_pad($kode1, 3, "0", STR_PAD_LEFT);
                                          } else {
                                           $kode_otomatis = "T001";
                                        }
                                        echo '<input type="text" id="id_transaksi" name="id_transaksi" class="form-control" value="'.$kode_otomatis.'"/>';
                                    ?> 
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-2">GRN Job Number</label>
                                <div class="col-md-4">
                                    <select class="form-control" type="text" id="id_grn" name="id_grn">
                                        <?php 
                                            while ($row2 = $query2->fetch_assoc()){
                                                echo '<option value="'.$row2['id_grn'].'">'.$row2['id_grn'].'</option>';
                                            }
                                        ?>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-2">MRF Job Number</label>
                                <div class="col-md-4">
                                    <select class="form-control" type="text" id="id_mrf" name="id_mrf">
                                        <?php 
                                            while ($row3 = $query3->fetch_assoc()){
                                                echo '<option value="'.$row3['id_mrf'].'">'.$row3['id_mrf'].'</option>';
                                            } 
                                        ?>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-2">Staff Number</label>
                                <div class="col-md-4">
                                    <select class="form-control" type="text" id="id_akun" name="id_akun">
                                        <?php 
                                            while ($row4 = $query4->fetch_assoc()){
                                                echo '<option value="'.$row4['id_akun'].'">'.$row4['id_akun'].'</option>';
                                            } 
                                        ?>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-2">Transaction Date</label>
                                <div class="col-md-4">
                                    <input type="date" class="form-control" name="trans">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-2">Jenis Transaksi DO</label>
                                <div class="col-md-4">
                                    <select class="form-control" type="text" id="jenis" name="jenis">
                                        <option value="KELUAR">KELUAR</option>
                                        <option value="MASUK">MASUK</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-2" >Status</label>
                                <div class="col-md-4">
                                    <input type="hidden" class="form-control" name="stat" value="PROCCED">
                                    <input type="text" class="form-control" name="stat" value="PROCCED" disabled>
                                </div>
                            </div>
                            <div class="form-group form-actions">
                                <div class="col-md-10 col-md-offset-2">
                                    <button class="btn btn-success" type="submit" name="simpan"><i class="fa fa-floppy-o"></i> Save</button>
                                    <a href="page-warehouse-culi.php" class="btn btn-danger"><i class="fa fa-times"></i> Cancel</a>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
<?php include 'footer.php'; ?>