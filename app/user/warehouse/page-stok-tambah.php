                <div id="page-content">
                    <ul id="nav-info" class="clearfix">
                        <li><a href="index.php"><i class="fa fa-home"></i></a></li>
                        <li><a href="javascript:void(0)">Transaksi</a></li>
                        <li><a href="javascript:void(0)">Stok Barang</a></li>
                        <li class="active"><a href="">Stok Barang Masuk</a></li>
                    </ul>
                    <?php 
                        require 'config/config.php';
                        $id = $_GET['tambah']; 
                        $query = mysqli_query($link, "SELECT * FROM detail_barang WHERE id_barang='$id'");
                        $row = mysqli_fetch_array($query);
                            if (count($row) == 1) {
                                $row = mysqli_fetch_array($query);
                            }
                    ?>
                    <form action="page-warehouse-stok-tambah-proses.php" method="POST" class="form-horizontal form-box">
                        <h4 class="form-box-header">Tambah Barang</h4>
                        <div class="form-box-content">
                            <div class="form-group">
                                <label class="control-label col-md-2">ID Barang</label>
                                <div class="col-md-2">
                                    <input type="hidden" class="form-control" name="id_barang" value="<?php echo $id; ?>">
                                    <input type="text" class="form-control" name="id_barang" value="<?php echo $id; ?>" disabled>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-2">Stok Tersedia</label>
                                <div class="col-md-2">
                                    <input type="text" class="form-control" name="stok" value="<?php echo $row['qty'] ?>">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-2" >Ditambahkan</label>
                                <div class="col-md-2">
                                    <input type="number" class="form-control" name="jum" value="">
                                </div>
                            </div>
                            <div class="form-group form-actions">
                                <div class="col-md-10 col-md-offset-2">
                                    <button class="btn btn-success" type="submit" name="tambah" onclick="return confirm(\'Yakin mau di simpan?\')"><i class="fa fa-floppy-o"></i> Update</button>
                                    <a href="page-warehouse-stok.php" class="btn btn-danger"><i class="fa fa-times"></i> Cancel</a>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
<?php include 'footer.php'; ?>