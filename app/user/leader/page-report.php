    <?php 
        require 'config/config.php';
        $query  = mysqli_query($link, "SELECT * FROM akun");
        $query2 = mysqli_query($link, "SELECT * FROM transaksi");
        $query3 = mysqli_query($link, "SELECT * FROM transaksi WHERE status='PROCCED'");
        $query4 = mysqli_query($link, "SELECT * FROM customer");
        $query5 = mysqli_query($link, "SELECT * FROM mrf");
        $query6 = mysqli_query($link, "SELECT * FROM mrf WHERE status='PROCCED'");
        $query7 = mysqli_query($link, "SELECT * FROM grn");
        $query8 = mysqli_query($link, "SELECT * FROM grn WHERE status='PROCCED'");
        $query9 = mysqli_query($link, "SELECT * FROM po");
        $query10= mysqli_query($link, "SELECT * FROM po WHERE status='PROCCED'");
    ?>

                <div id="page-content">
                    <ul id="nav-info" class="clearfix">
                        <li><a href="index.html"><i class="fa fa-home"></i></a></li>
                        <li class="active"><a href="">Dashboard</a></li>
                    </ul>
                    <ul class="nav-dash">
        <li>
            <a href="page-leader-report-stok-print.php" data-toggle="tooltip" title="Laporan Stok Barang" class="animation-fadeIn">
                <i class="fa fa-paperclip"></i>
            </a>
        </li>
        <li>
            <a href="page-leader-report-keluar-print.php" data-toggle="tooltip" title="Laporan Transaksi Keluar" class="animation-fadeIn">
                <i class="fa fa-tasks"></i> <span class="badge badge-warning"></span>
            </a>
        </li>
        <li>
            <a href="page-leader-report-masuk-print.php" data-toggle="tooltip" title="Laporan Transaksi Masuk" class="animation-fadeIn">
                <i class="fa fa-book"></i>
            </a>
        </li>
    </ul>
    <!-- END Nav Dash -->
                    <div class="dash-tiles row">
                        <!-- Column 1 of Row 1 -->
                        <div class="col-sm-1"></div>
                        <div class="col-sm-3">
                            <!-- Total Users Tile -->
                            <div class="dash-tile dash-tile-ocean clearfix animation-pullDown">
                                <div class="dash-tile-header">
                                    Total Users
                                </div>
                                <div class="dash-tile-icon"><i class="fa fa-user"></i></div>
                                <div class="dash-tile-text"><?php echo mysqli_num_rows($query);?> User</div>
                            </div>
                            <div class="dash-tile dash-tile-leaf clearfix animation-pullDown">
                                <div class="dash-tile-header">
                                    Total Customer
                                </div>
                                <div class="dash-tile-icon"><i class="fa fa-users"></i></div>
                                <div class="dash-tile-text"><?php echo mysqli_num_rows($query4);?> Customer</div>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="dash-tile dash-tile-flower clearfix animation-pullDown">
                                <div class="dash-tile-header">
                                    Total Transaction : <?php echo mysqli_num_rows($query2);?>
                                </div>
                                <div class="dash-tile-icon"><i class="fa fa-tags"></i></div>
                                <div class="dash-tile-text"><?php echo mysqli_num_rows($query3);?> Not Done</div>
                            </div>
                            <div class="dash-tile dash-tile-fruit clearfix animation-pullDown">
                                <div class="dash-tile-header">
                                    Total PO : <?php echo mysqli_num_rows($query9);?>
                                </div>
                                <div class="dash-tile-icon"><i class="fa fa-cloud-download"></i></div>
                                <div class="dash-tile-text"><?php echo mysqli_num_rows($query10);?> Not Done</div>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="dash-tile dash-tile-oil clearfix animation-pullDown">
                                <div class="dash-tile-header">
                                    Total MRF <?php echo mysqli_num_rows($query5); ?>
                                </div>
                                <div class="dash-tile-icon"><i class="fa fa-globe"></i></div>
                                <div class="dash-tile-text"><?php echo mysqli_num_rows($query6); ?> Not Done</div>
                            </div>
                            <div class="dash-tile dash-tile-dark clearfix animation-pullDown">
                                <div class="dash-tile-header">
                                    Total GRN : <?php echo mysqli_num_rows($query7); ?>
                                </div>
                                <div class="dash-tile-icon"><i class="gi gi-cargo"></i></div>
                                <div class="dash-tile-text"><?php echo mysqli_num_rows($query8); ?> Not Done</div>
                            </div>
                        </div>
                    </div>
                </div>
                <?php include 'footer.php'?>