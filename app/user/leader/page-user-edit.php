<div id="page-content">
    <ul id="nav-info" class="clearfix">
        <li><a href="index.php"><i class="fa fa-home"></i></a></li>
        <li><a href="javascript:void(0)">Transaksi</a></li>
        <li><a href="javascript:void(0)">Customer</a></li>
        <li class="active"><a href="">Tambah User</a></li>
    </ul>
    <?php 
        require 'config/config.php';
        $id = $_GET['edit']; 
        $query = mysqli_query($link, "SELECT * FROM akun WHERE id_akun='$id'");
        $row = mysqli_fetch_array($query);
        if (count($row) == 1) {
            $row = mysqli_fetch_array($query);
        }
    ?>
    <form action="page-leader-akun-edit-proses.php" method="POST" class="form-horizontal form-box">
        <h4 class="form-box-header">Text Inputs</h4>
        <div class="form-box-content">
            <div class="form-group">
                <div class="col-md-3">
                    <div class="input-group">
                        <span class="input-group-addon">ID :</span>
                        <input type="hidden" name="id_akun" class="form-control" value="<?php echo $row['id_akun']; ?>">
                        <input type="text" name="id_akun" class="form-control" value="<?php echo $row['id_akun']; ?>" disabled>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="input-group">
                        <span class="input-group-addon">Username :</span>
                        <input type="text" name="user" class="form-control" value="<?php echo $row['username']; ?>">
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="input-group">
                        <span class="input-group-addon">Password :</i></span>
                        <input type="text" name="pass" class="form-control" value="<?php echo $row['password']; ?>">
                    </div>
                </div>
            </div>
            <div class="form-group">
                <div class="col-md-3">
                    <div class="input-group">
                        <span class="input-group-addon">Nama :</span>
                        <input type="text" name="nama" class="form-control" value="<?php echo $row['nama_akun']; ?>">
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="input-group">
                        <span class="input-group-addon">Tempat Lahir</span>
                        <input type="text" name="tmp" class="form-control" value="<?php echo $row['tmp_lhr']; ?>">
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="input-group">
                        <span class="input-group-addon">Tanggal Lahir :</span>
                        <input type="date" name="tgl" class="form-control" value="<?php echo $row['tgl_lhr']; ?>">
                    </div>
                </div>
            </div>
            <div class="form-group">
                <div class="col-md-3">
                    <div class="input-group">
                        <span class="input-group-addon"><i class="fa fa-mobile"></i>  Contack :</span>
                        <input type="text" name="telp" class="form-control" value="<?php echo $row['telp_akun']; ?>">
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="input-group">
                        <span class="input-group-addon">@</span>
                        <input type="text" name="email" class="form-control" value="<?php echo $row['email_akun']; ?>">
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="input-group">
                        <span class="input-group-addon">Departemen :</span>
                        <input type="text" name="dept" class="form-control" value="<?php echo $row['dept']; ?>">
                    </div>
                </div>
            </div>
            <div class="form-group">
                <div class="col-md-3">
                    <div class="input-group">
                        <span class="input-group-addon">Devisi :</span>
                        <input type="text" name="dev" class="form-control" value="<?php echo $row['dev']; ?>">
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="input-group">
                        <span class="input-group-addon">Jabatan :</span>
                        <input type="text" name="jab" class="form-control" value="<?php echo $row['jab']; ?>">
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="input-group">
                        <span class="input-group-addon">Permission :</span>
                        <input type="hidden" name="perm" class="form-control" value="<?php echo $row['perm']; ?>">
                        <input type="text" name="perm" class="form-control" value="<?php echo $row['perm']; ?>" disabled>
                    </div>
                </div>
            </div>
            <div class="form-group form-actions">
                <div class="col-md-10 col-md-offset-2">
                    <button class="btn btn-success" type="submit" name="update"><i class="fa fa-floppy-o"></i> Save</button>
                    <a href="page-leader-akun.php" class="btn btn-danger"><i class="fa fa-times"></i> Cancel</a>
                </div>
            </div>
        </div>
    </form>
</div>
<?php include 'footer.php'; ?>