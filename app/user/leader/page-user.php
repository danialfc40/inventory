<?php 
    require 'config/config.php';
    if (!$link) {
        die ('Failed to connect to MySQL: ' . mysqli_connect_error());  
    }
    $sql = 'SELECT * FROM akun';  
    $query = mysqli_query($link, $sql);
    if (!$query) {
        die ('SQL Error: ' . mysqli_error($conn));
    }
?>
<!-- Page Content -->
<div id="page-content">
    <!-- Navigation info -->
    <ul id="nav-info" class="clearfix">
        <li><a href="index.php"><i class="fa fa-home"></i></a></li>
        <li class="active"><a href="">Account</a></li>
    </ul>
    <!-- END Navigation info -->

    <!-- Editable Datatables -->
    <h3 class="page-header page-header-top">Data Table <small>Account.</small></h3>
    <div class="dash-tiles row">
            <!-- Column 1 of Row 1 -->

        <div class="col-sm-12">
            <div class="push">
                <a href="page-leader-akun-tambah.php" class="btn btn-success"><i class="fa fa-plus"></i> Add Account</a>
            </div>
            <!-- Table -->
            <table id="example-editable-datatables" class="table table-bordered table-hover">
                <thead>
                    <tr>
                        <th class="cell-small text-center">No.</th>
                        <th> ID Akun</th>
                        <th class="hidden-xs hidden-sm"> Username</th>
                        <th class="hidden-xs hidden-sm"> Password</th>
                        <th class="hidden-xs hidden-sm"> Name</th>
                        <th> Tempat Lahir</th>
                        <th class="hidden-xs hidden-sm"> Tanggal Lahir</th>
                        <th class="hidden-xs hidden-sm"> Telepon</th>
                        <th class="hidden-xs hidden-sm"> Email</th>
                        <th> Departemen</th>
                        <th class="hidden-xs hidden-sm"> Devisi</th>
                        <th class="hidden-xs hidden-sm"> Jabatan</th>
                        <th class="hidden-xs hidden-sm"> Permision</th>
                        <th class="cell-small"></th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                        $no = 1;
                        while ($row = mysqli_fetch_array($query)) {
                            echo '<tr>
                                    <td>'.$no++.'</td>
                                    <td>'.$row['id_akun'].'</td>
                                    <td>'.$row['username'].'</td>
                                    <td>'.$row['password'].'</td>
                                    <td>'.$row['nama_akun'].'</td>
                                    <td>'.$row['tmp_lhr'].'</td>
                                    <td>'.$row['tgl_lhr'].'</td>
                                    <td>'.$row['telp_akun'].'</td>
                                    <td>'.$row['email_akun'].'</td>
                                    <td>'.$row['dept'].'</td>
                                    <td>'.$row['dev'].'</td>
                                    <td>'.$row['jab'].'</td>
                                    <td>'.$row['perm'].'</td>
                                    <td><a href="page-leader-akun-edit.php?edit='.$row['id_akun'].'" class= "btn btn-xs btn-success"><i class="fa fa-pencil"></i></a> <a href="page-leader-akun-delete.php?delete='.$row['id_akun'].'" class= "btn btn-xs btn-danger"><i class="fa fa-times-circle"></i></a></td>
                                </tr>';
                        }
                    ?>
                </tbody>
            </table>
            <!-- END Table -->
            <!-- END Editable Datatables -->
            <!-- Javascript code only for this page -->
        </div>
    </div>
</div>
<!-- END Page Content -->
<?php include 'footer.php'; ?>

