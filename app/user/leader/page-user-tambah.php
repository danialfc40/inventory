<div id="page-content">
    <ul id="nav-info" class="clearfix">
        <li><a href="index.php"><i class="fa fa-home"></i></a></li>
        <li><a href="javascript:void(0)">Transaksi</a></li>
        <li><a href="javascript:void(0)">Customer</a></li>
        <li class="active"><a href="">Tambah User</a></li>
    </ul>
    <form action="page-leader-akun-tambah-proses.php" method="POST" class="form-horizontal form-box">
        <h4 class="form-box-header">Text Inputs</h4>
        <div class="form-box-content">
            <div class="form-group">
                <div class="col-md-3">
                    <div class="input-group">
                        <span class="input-group-addon">ID :</span>
                        <?php 
                            require 'config/config.php';
                            if (!$link) {
                                die ('Failed to connect to MySQL: ' . mysqli_connect_error());  
                            }  
                            $sql = 'SELECT * FROM akun'; 
                            $carikode = mysqli_query($link, "SELECT max(id_akun) FROM akun");
                            $datakode = mysqli_fetch_array($carikode);
                            $query = mysqli_query($link, $sql);
                            if (!$query) {
                                die ('SQL Error: ' . mysqli_error($conn));
                            }
                            if ($datakode) {
                               $nilaikode = substr($datakode[0], 1);
                               // menjadikan $nilaikode ( int )
                               $kode = (int) $nilaikode;
                               // setiap $kode di tambah 1
                               $kode1 = $kode + 1;
                               $kode_otomatis = "U".str_pad($kode1, 3, "0", STR_PAD_LEFT);
                              } else {
                               $kode_otomatis = "U01";
                            }
                            echo '<input type="text" name="id_akun" class="form-control" value="'.$kode_otomatis.'"/>';
                        ?> 
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="input-group">
                        <span class="input-group-addon">Username :</span>
                        <input type="text" name="user" class="form-control">
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="input-group">
                        <span class="input-group-addon">Password :</i></span>
                        <input type="text" name="pass" class="form-control">
                    </div>
                </div>
            </div>
            <div class="form-group">
                <div class="col-md-3">
                    <div class="input-group">
                        <span class="input-group-addon">Nama :</span>
                        <input type="text" name="nama" class="form-control">
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="input-group">
                        <span class="input-group-addon">Tempat Lahir</span>
                        <input type="text" name="tmp" class="form-control">
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="input-group">
                        <span class="input-group-addon">Tanggal Lahir :</span>
                        <input type="date" name="tgl" class="form-control">
                    </div>
                </div>
            </div>
            <div class="form-group">
                <div class="col-md-3">
                    <div class="input-group">
                        <span class="input-group-addon"><i class="fa fa-mobile"></i>  Contack :</span>
                        <input type="text" name="telp" class="form-control">
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="input-group">
                        <span class="input-group-addon">@</span>
                        <input type="text" name="email" class="form-control">
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="input-group">
                        <span class="input-group-addon">Departemen :</span>
                        <input type="text" name="dept" class="form-control">
                    </div>
                </div>
            </div>
            <div class="form-group">
                <div class="col-md-3">
                    <div class="input-group">
                        <span class="input-group-addon">Devisi :</span>
                        <input type="text" name="dev" class="form-control">
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="input-group">
                        <span class="input-group-addon">Jabatan :</span>
                        <input type="text" name="jab" class="form-control">
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="input-group">
                        <span class="input-group-addon">Permission :</span>
                        <select class="form-control" name="perm">
                            <option value="part specialist">part specialist</option>
                            <option value="warehose man">warehose man</option>
                            <option value="service sales leader">service sales leader</option>
                        </select>
                    </div>
                </div>
            </div>
            <div class="form-group form-actions">
                <div class="col-md-10 col-md-offset-2">
                    <button class="btn btn-success" type="submit" name="simpan"><i class="fa fa-floppy-o"></i> Save</button>
                    <a href="page-leader-akun.php" class="btn btn-danger"><i class="fa fa-times"></i> Cancel</a>
                </div>
            </div>
        </div>
    </form>
</div>
<?php include 'footer.php'; ?>