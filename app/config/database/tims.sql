-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Aug 07, 2018 at 09:19 PM
-- Server version: 10.1.29-MariaDB
-- PHP Version: 7.2.0

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `tims`
--

-- --------------------------------------------------------

--
-- Table structure for table `akun`
--

CREATE TABLE `akun` (
  `id_akun` varchar(12) NOT NULL,
  `username` varchar(25) NOT NULL,
  `password` varchar(25) NOT NULL,
  `nama_akun` varchar(25) NOT NULL,
  `tmp_lhr` varchar(25) NOT NULL,
  `tgl_lhr` date NOT NULL,
  `telp_akun` varchar(25) NOT NULL,
  `email_akun` varchar(25) NOT NULL,
  `dept` varchar(25) NOT NULL,
  `dev` varchar(25) NOT NULL,
  `jab` varchar(25) NOT NULL,
  `perm` enum('part specialist','finance','warehose man','sales engineer','service sales leader') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `akun`
--

INSERT INTO `akun` (`id_akun`, `username`, `password`, `nama_akun`, `tmp_lhr`, `tgl_lhr`, `telp_akun`, `email_akun`, `dept`, `dev`, `jab`, `perm`) VALUES
('U001', 'yahoo', 'yahoo', 'damar', 'fgk', '2018-07-03', '4354ryt', 'tfhg', 'fghg', 'dgfh', 'sdgf', 'warehose man'),
('U002', 'admin', 'admin', 'danial', 'surakarta', '2018-07-04', '087836695995', 'danialfc40@gmail.com', 'gudang', 'warehouse man', 'manajer', 'part specialist'),
('U003', 'wew', 'wew', 'JLN', 'LJN', '2018-07-03', ';O', ';OJ', ';OJ', ';OJ', 'LN', 'service sales leader');

-- --------------------------------------------------------

--
-- Table structure for table `barang`
--

CREATE TABLE `barang` (
  `id_barang` varchar(25) NOT NULL,
  `nama_barang` varchar(25) DEFAULT NULL,
  `description` varchar(25) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `barang`
--

INSERT INTO `barang` (`id_barang`, `nama_barang`, `description`) VALUES
('SE01', 'KIPAS', 'ANGIN');

-- --------------------------------------------------------

--
-- Table structure for table `customer`
--

CREATE TABLE `customer` (
  `id_customer` varchar(25) NOT NULL,
  `nama_customer` varchar(25) DEFAULT NULL,
  `alamat_customer` varchar(25) DEFAULT NULL,
  `company` varchar(25) DEFAULT NULL,
  `email` varchar(25) DEFAULT NULL,
  `telp` varchar(25) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `customer`
--

INSERT INTO `customer` (`id_customer`, `nama_customer`, `alamat_customer`, `company`, `email`, `telp`) VALUES
('C004', 'danial', 'knn', 'kln', 'kln', 'lkn');

-- --------------------------------------------------------

--
-- Table structure for table `detail_barang`
--

CREATE TABLE `detail_barang` (
  `id_barang` varchar(25) DEFAULT NULL,
  `qty` int(25) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `detail_barang`
--

INSERT INTO `detail_barang` (`id_barang`, `qty`) VALUES
('SE01', 162);

-- --------------------------------------------------------

--
-- Table structure for table `detail_do`
--

CREATE TABLE `detail_do` (
  `id_do` varchar(25) DEFAULT NULL,
  `id_barang` varchar(25) DEFAULT NULL,
  `qty` varchar(25) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `detail_do`
--

INSERT INTO `detail_do` (`id_do`, `id_barang`, `qty`) VALUES
('DO003', 'idoo0', '9'),
('DO004', 'SE01', '9'),
('DO005', 'SE02', '78'),
('DO006', 'se1', '90'),
('DO007', 'sj', 'n'),
('DO008', 'se', '8');

-- --------------------------------------------------------

--
-- Table structure for table `detail_grn`
--

CREATE TABLE `detail_grn` (
  `id_grn` varchar(25) DEFAULT NULL,
  `id_barang` varchar(25) DEFAULT NULL,
  `qty` varchar(25) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `detail_grn`
--

INSERT INTO `detail_grn` (`id_grn`, `id_barang`, `qty`) VALUES
('GR001', 'se1', '90'),
('GR002', '89', '99');

-- --------------------------------------------------------

--
-- Table structure for table `detail_mrf`
--

CREATE TABLE `detail_mrf` (
  `id_mrf` varchar(25) DEFAULT NULL,
  `id_barang` varchar(25) DEFAULT NULL,
  `qty` varchar(25) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `detail_mrf`
--

INSERT INTO `detail_mrf` (`id_mrf`, `id_barang`, `qty`) VALUES
('MR001', 'SE01', '9'),
('MR001', 'SE01', '9'),
('MR002', 'SE01', '78'),
('MR003', 'uu', '8');

-- --------------------------------------------------------

--
-- Table structure for table `detail_po`
--

CREATE TABLE `detail_po` (
  `id_po` varchar(25) DEFAULT NULL,
  `id_barang` varchar(25) DEFAULT NULL,
  `qty` varchar(25) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `detail_po`
--

INSERT INTO `detail_po` (`id_po`, `id_barang`, `qty`) VALUES
('PO001', 'se01', '90'),
('PO002', '89', '88'),
('PO003', 'se01', '8');

-- --------------------------------------------------------

--
-- Table structure for table `detail_ro`
--

CREATE TABLE `detail_ro` (
  `id_ro` varchar(12) NOT NULL,
  `id_barang` varchar(25) NOT NULL,
  `qty` varchar(25) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `detail_ro`
--

INSERT INTO `detail_ro` (`id_ro`, `id_barang`, `qty`) VALUES
('RO001', 'SE01', '90'),
('RO001', 'SE02', '90'),
('RO001', 'SE01', '5'),
('RO002', 'se01', '90'),
('RO002', 'tahii', '9'),
('RO003', 'SE01', '7'),
('RO004', 'se09', 'jb'),
('RO005', ',JB', 'JB');

-- --------------------------------------------------------

--
-- Table structure for table `do`
--

CREATE TABLE `do` (
  `id_do` varchar(25) NOT NULL,
  `id_ro` varchar(25) DEFAULT NULL,
  `id_po` varchar(25) DEFAULT NULL,
  `tgl_do` date DEFAULT NULL,
  `jenis_do` varchar(25) DEFAULT NULL,
  `status` varchar(25) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `do`
--

INSERT INTO `do` (`id_do`, `id_ro`, `id_po`, `tgl_do`, `jenis_do`, `status`) VALUES
('DO003', 'RO003', NULL, '2018-07-31', 'MASUK', 'DONE'),
('DO004', '-', 'PO001', '2018-08-08', 'KELUAR', 'DONE'),
('DO005', '-', 'PO002', '2018-08-10', 'KELUAR', 'DONE'),
('DO006', 'RO002', '-', '2018-08-08', 'MASUK', 'DONE'),
('DO007', 'RO005', '-', '0000-00-00', 'MASUK', 'DONE'),
('DO008', '-', 'PO003', '2018-08-31', 'KELUAR', 'DONE');

-- --------------------------------------------------------

--
-- Table structure for table `grn`
--

CREATE TABLE `grn` (
  `id_grn` varchar(25) NOT NULL,
  `id_do` varchar(25) NOT NULL,
  `id_akun` varchar(25) NOT NULL,
  `tgl_grn` date NOT NULL,
  `status` varchar(25) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `grn`
--

INSERT INTO `grn` (`id_grn`, `id_do`, `id_akun`, `tgl_grn`, `status`) VALUES
('GR001', 'DO006', 'U001', '2018-08-10', 'DONE'),
('GR002', 'DO007', 'U001', '2018-08-11', 'PROCCED');

-- --------------------------------------------------------

--
-- Table structure for table `mrf`
--

CREATE TABLE `mrf` (
  `id_mrf` varchar(25) NOT NULL,
  `id_do` varchar(25) DEFAULT NULL,
  `id_akun` varchar(25) DEFAULT NULL,
  `tgl_mrf` date DEFAULT NULL,
  `status` varchar(25) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `mrf`
--

INSERT INTO `mrf` (`id_mrf`, `id_do`, `id_akun`, `tgl_mrf`, `status`) VALUES
('MR001', 'DO004', 'U001', '2018-08-24', 'DONE'),
('MR002', 'DO005', 'U001', '2018-08-01', 'PROCCED'),
('MR003', 'DO008', 'U001', '2018-08-09', 'PROCCED');

-- --------------------------------------------------------

--
-- Table structure for table `po`
--

CREATE TABLE `po` (
  `id_po` varchar(25) NOT NULL,
  `id_customer` varchar(25) DEFAULT NULL,
  `tgl_po` date DEFAULT NULL,
  `status` varchar(25) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `po`
--

INSERT INTO `po` (`id_po`, `id_customer`, `tgl_po`, `status`) VALUES
('PO001', 'C004', '2018-08-08', 'DONE'),
('PO002', 'C004', '2018-08-08', 'DONE'),
('PO003', 'C004', '2018-08-17', 'DONE');

-- --------------------------------------------------------

--
-- Table structure for table `ro`
--

CREATE TABLE `ro` (
  `id_ro` varchar(12) NOT NULL,
  `tgl_ro` date DEFAULT NULL,
  `satus` varchar(12) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ro`
--

INSERT INTO `ro` (`id_ro`, `tgl_ro`, `satus`) VALUES
('RO001', '2018-08-09', 'DONE'),
('RO002', '0000-00-00', 'DONE'),
('RO003', '2018-07-31', 'DONE'),
('RO004', '2018-08-01', 'DONE'),
('RO005', '2018-08-09', 'DONE');

-- --------------------------------------------------------

--
-- Table structure for table `transaksi`
--

CREATE TABLE `transaksi` (
  `id_transaksi` varchar(25) NOT NULL,
  `id_grn` varchar(25) DEFAULT NULL,
  `id_mrf` varchar(25) DEFAULT NULL,
  `id_akun` varchar(25) DEFAULT NULL,
  `tgl_transaksi` date DEFAULT NULL,
  `jenis_transaksi` varchar(25) DEFAULT NULL,
  `status` varchar(25) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `transaksi`
--

INSERT INTO `transaksi` (`id_transaksi`, `id_grn`, `id_mrf`, `id_akun`, `tgl_transaksi`, `jenis_transaksi`, `status`) VALUES
('T001', '-', 'MR001', 'U001', '2018-08-15', 'KELUAR', 'DONE'),
('T002', 'GR001', '-', 'U001', '2018-08-23', 'MASUK', 'DONE');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `akun`
--
ALTER TABLE `akun`
  ADD PRIMARY KEY (`id_akun`);

--
-- Indexes for table `barang`
--
ALTER TABLE `barang`
  ADD PRIMARY KEY (`id_barang`) USING BTREE;

--
-- Indexes for table `customer`
--
ALTER TABLE `customer`
  ADD PRIMARY KEY (`id_customer`);

--
-- Indexes for table `detail_barang`
--
ALTER TABLE `detail_barang`
  ADD KEY `id_barang` (`id_barang`);

--
-- Indexes for table `do`
--
ALTER TABLE `do`
  ADD PRIMARY KEY (`id_do`);

--
-- Indexes for table `mrf`
--
ALTER TABLE `mrf`
  ADD PRIMARY KEY (`id_mrf`);

--
-- Indexes for table `po`
--
ALTER TABLE `po`
  ADD PRIMARY KEY (`id_po`);

--
-- Indexes for table `ro`
--
ALTER TABLE `ro`
  ADD PRIMARY KEY (`id_ro`);

--
-- Indexes for table `transaksi`
--
ALTER TABLE `transaksi`
  ADD PRIMARY KEY (`id_transaksi`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
