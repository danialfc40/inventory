<?php include 'login_proses.php'; ?>
<!DOCTYPE html>
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <title>Trane Inventory Management System</title>
        <meta name="description" content="uAdmin is a Professional, Responsive and Flat Admin Template created by pixelcave and published on Themeforest">
        <meta name="author" content="pixelcave">
        <meta name="robots" content="noindex, nofollow">
        <meta name="viewport" content="width=device-width,initial-scale=1">
        <link rel="shortcut icon" href="img/favicon.ico">
        <link rel="apple-touch-icon" href="img/icon57.png" sizes="57x57">
        <link rel="apple-touch-icon" href="img/icon72.png" sizes="72x72">
        <link rel="apple-touch-icon" href="img/icon76.png" sizes="76x76">
        <link rel="apple-touch-icon" href="img/icon114.png" sizes="114x114">
        <link rel="apple-touch-icon" href="img/icon120.png" sizes="120x120">
        <link rel="apple-touch-icon" href="img/icon144.png" sizes="144x144">
        <link rel="apple-touch-icon" href="img/icon152.png" sizes="152x152">
        <link rel="stylesheet" href="css/bootstrap.css">
        <link rel="stylesheet" href="css/plugins.css">
        <link rel="stylesheet" href="css/main.css">
        <script src="js/vendor/modernizr-respond.min.js"></script>
    </head>
    <body class="login">
        <div id="login-container">
            <div id="login-logo">
                <a href="">
                    <img src="img/template/uadmin_logo.png" alt="logo">
                </a>
            </div>
            <div id="login-buttons">
                <h5 class="page-header-sub">Login with..</h5>
                <button id="login-btn-email" class="btn btn-default">Login.. <i class="fa fa-envelope"></i></button>
            </div>
            <form id="login-form" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>" method="post" class="form-horizontal">
                <div class="form-group">
                    <a href="javascript:void(0)" class="login-back"><i class="fa fa-arrow-left"></i></a>
                </div>
                <div class="form-group">
                    <div class="col-xs-12">
                        <div class="input-group <?php echo (!empty($username_err)) ? 'has-error' : ''; ?>">
                            <input type="text" id="username" name="username" placeholder="Email.." class="form-control" value="<?php echo $username; ?>">
                            <span class="input-group-addon"><?php echo $username_err; ?><i class="fa fa-envelope-o fa-fw"></i></span>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-xs-12">
                        <div class="input-group <?php echo (!empty($password_err)) ? 'has-error' : ''; ?>">
                            <input type="password" id="password" name="password" placeholder="Password.." class="form-control">
                            <span class="input-group-addon"><?php echo $password_err; ?><i class="fa fa-asterisk fa-fw"></i></span>
                        </div>
                    </div>
                </div>
                <div class="clearfix">
                    <div class="btn-group btn-group-sm pull-right">
                        <button type="submit" name="login" class="btn btn-success"><i class="fa fa-arrow-right"></i> Login</button>
                    </div>
                </div>
            </form>
        </div>
    </body>
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    <script>!window.jQuery && document.write(decodeURI('%3Cscript src="js/vendor/jquery-1.11.1.min.js"%3E%3C/script%3E'));</script>
    <script src="js/vendor/bootstrap.min.js"></script>
    <script src="js/plugins.js"></script>
    <script src="js/main.js"></script>
    <script>
        $(function () {
            var loginButtons = $('#login-buttons');
            var loginForm = $('#login-form');
            $('#login-btn-email').click(function () {
                loginButtons.slideUp(600);
                loginForm.slideDown(450);
            });
            $('.login-back').click(function () {
                loginForm.slideUp(450);
                loginButtons.slideDown(600);
            });
        });
    </script>
</html>